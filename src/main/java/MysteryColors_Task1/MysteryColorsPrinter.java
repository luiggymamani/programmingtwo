package MysteryColors_Task1;

import java.awt.Color;
import java.util.List;

public class MysteryColorsPrinter {

    public void printNumberOfDistinctColors(int numberOfDistinctColors, List<Color> colors){
        System.out.println("Number Of Distinct Colors of the List : " + numberOfDistinctColors);
        System.out.println("Colors: ");
        printColors(colors);
    }

    public void printColors(List<Color> colors){
        for (Color color : colors) {
            System.out.println("color");
        }
    }

    public void printColorOccurrence(int numberOfTimesOfTheColor, Color color){
        System.out.println("Color : " + color);
        System.out.println("Number Of Times Of The Color : " + numberOfTimesOfTheColor);
    }
}
