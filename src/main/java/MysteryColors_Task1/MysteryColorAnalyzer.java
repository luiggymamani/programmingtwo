package MysteryColors_Task1;

import java.util.ArrayList;
import java.util.List;



public class MysteryColorAnalyzer implements MysteryColorAnalyzerable {

    private List<Color> mysteryColors;
    private List<Color> distinctColors;

    public MysteryColorAnalyzer(){
        this.mysteryColors = new ArrayList<>();
        this.distinctColors = new ArrayList<>();
    }

    public void addColorToMysteryColors(Color color){
        mysteryColors.add(color);
    }

    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        distinctColors.clear();
        addColorsToDistinctColors(mysteryColors);
        return distinctColors.size();
    }

    private void addColorsToDistinctColors(List<Color> mysteryColors){
        for (Color color: mysteryColors) {
            if (!colorRepeat(color, distinctColors)){
                distinctColors.add(color);
            }
        }
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int numberOfTimesOfTheColor = 0;
        for (Color colorToCompare : mysteryColors) {
            if (color.equals(colorToCompare)){
                numberOfTimesOfTheColor += 1;
            }
        }
        return numberOfTimesOfTheColor;
    }


    private boolean colorRepeat(Color color, List<Color> colors){
        boolean colorIsRepeat = false;
        for (Color colorToCompare : colors) {
            if (color.equals(colorToCompare)) {
                colorIsRepeat = true;
                break;
            }
        }
        return colorIsRepeat;
    }

    public List<Color> getMysteryColors() {
        return mysteryColors;
    }

    public List<Color> getDistinctColors() {
        return distinctColors;
    }

    public void setMysteryColors(List<Color> mysteryColors) {
        this.mysteryColors = mysteryColors;
    }

    public void setDistinctColors(List<Color> distinctColors) {
        this.distinctColors = distinctColors;
    }
}
