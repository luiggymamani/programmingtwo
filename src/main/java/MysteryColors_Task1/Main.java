package MysteryColors_Task1;

public class Main {

    public static void main(String[] args){

        MysteryColorAnalyzer mysteryColorAnalyzer = new MysteryColorAnalyzer();
        MysteryColorsPrinter mysteryColorsPrinter = new MysteryColorsPrinter();

        mysteryColorAnalyzer.addColorToMysteryColors(Color.GREEN);
        mysteryColorAnalyzer.addColorToMysteryColors(Color.GREEN);
        mysteryColorAnalyzer.addColorToMysteryColors(Color.GREEN);

        mysteryColorAnalyzer.addColorToMysteryColors(Color.RED);
        mysteryColorAnalyzer.addColorToMysteryColors(Color.RED);
        mysteryColorAnalyzer.addColorToMysteryColors(Color.RED);
        mysteryColorAnalyzer.addColorToMysteryColors(Color.RED);

        mysteryColorAnalyzer.addColorToMysteryColors(Color.BLUE);
        mysteryColorAnalyzer.addColorToMysteryColors(Color.BLUE);

        System.out.print("Number of Distinct Colors : ");
        System.out.println(mysteryColorAnalyzer.numberOfDistinctColors(mysteryColorAnalyzer.getMysteryColors()));
        System.out.println();

        System.out.print("Color Green Occurrence: ");
        System.out.println(mysteryColorAnalyzer.colorOccurrence(mysteryColorAnalyzer.getMysteryColors(), Color.GREEN));

        System.out.print("Color Red Occurrence: ");
        System.out.println(mysteryColorAnalyzer.colorOccurrence(mysteryColorAnalyzer.getMysteryColors(), Color.RED));

        System.out.print("Color Blue Occurrence: ");
        System.out.println(mysteryColorAnalyzer.colorOccurrence(mysteryColorAnalyzer.getMysteryColors(), Color.BLUE));

    }
}
