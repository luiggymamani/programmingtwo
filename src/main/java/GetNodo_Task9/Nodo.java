package GetNodo_Task9;

public class Nodo {

    private int nodeData;
    private Nodo nextNode;

    public Nodo(int dataNode) {
        this.nodeData = dataNode;
        nextNode = null;
    }

    public Nodo(){
        nextNode = null;
    }

    public static int getNth(Nodo node, int index){
        int nodeData = node.getNodeData();

        for (int count = 0; count < index; count++) {
            node = node.getNextNode();
            nodeData = node.getNodeData();
        }
        return nodeData;
    }

    public static int calculateLength(Nodo mainNode){
        int length = 0;
        while (mainNode != null){
            mainNode = mainNode.getNextNode();
            length ++;
        }
        return length;
    }

    public int getNodeData() {
        return nodeData;
    }

    public Nodo getNextNode() {
        return nextNode;
    }

    public void setNextNode(Nodo nextNode) {
        this.nextNode = nextNode;
    }
}
