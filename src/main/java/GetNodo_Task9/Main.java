package GetNodo_Task9;

public class Main {

    public static void main(String[] args) {
        Nodo mainNode = new Nodo(1337);
        Nodo secondNode = new Nodo(42);
        Nodo thirdNode = new Nodo(23);
        mainNode.setNextNode(secondNode);
        mainNode.getNextNode().setNextNode(thirdNode);

        int index;

        System.out.println("------------First Case------------");
        index = 2;
        if (index < Nodo.calculateLength(mainNode)){
            System.out.println("Index: " + index + "\nValue: " + Nodo.getNth(mainNode, index));
        } else {
            System.out.println("Index out of range");
        }

        System.out.println("------------Second Case------------");
        index = 124;
        if (index < Nodo.calculateLength(mainNode)){
            System.out.println("Index: " + index + "\nValue: " + Nodo.getNth(mainNode, index));
        } else {
            System.out.println("Index: " + index + "\nIndex out of range");
        }
    }
}
