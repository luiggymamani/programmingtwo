package Clase25_08;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        list.add(new Student("Std 2", 80));
        list.add(new Student("Std 4", 70));
        list.add(new Student("Std 1", 90));
        list.add(new Student("Std 1", 50));

        list.sort(Comparator.naturalOrder());
        for (Student s: list ) {
            System.out.println(s);
        }
    }
}
