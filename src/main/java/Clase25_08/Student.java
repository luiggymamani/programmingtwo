package Clase25_08;

public class Student implements Comparable<Student>{
        String name;
        int score;
        public Student(String name, int score) {
            this.name = name;
            this.score = score;
        }
        public int compareTo(Student o) {
            int value = name.compareTo(o.name);

            if (value == 0) {
                value = Integer.compare(score, o.score);
            }
            return value;
        }
        public String toString() {
            return "name:" + name + ", score: " + score;
        }


}
