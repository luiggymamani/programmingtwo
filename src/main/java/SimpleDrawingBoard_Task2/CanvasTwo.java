package SimpleDrawingBoard_Task2;

public class CanvasTwo {

    private int width;
    private int height;
    private StringBuilder board;
    private final String space;
    private final String verticalLine;
    private final String horizontalLine;
    private final char symbol;

    public CanvasTwo(int width, int height) {
        this.width = width;
        this.height = height;

        space = " ";
        symbol = 'x';
        verticalLine = "|";
        horizontalLine = "-";
    }

    public void createContour(){
        String board;
        board = horizontalLine.repeat(width + 2) + "\n";
        for (int i = 0; i < height; i++) {
            board += verticalLine + space.repeat(width) + verticalLine + "\n";
        }
        board += horizontalLine.repeat(width + 2) + "\n";

        this.board = new StringBuilder(board);
    }

    public StringBuilder getBoard() {
        return board;
    }

    public void draw(int index, char character){
        board.setCharAt(index, character);
    }

    public void printBoard(){
        System.out.println(board);
    }

    public void generateDraw(int index, int amountX, int amountY){
        int indexController = index;

        for (int i = 0; i < amountX; i++) {
            draw(index + i, symbol);
            indexController += i;
        }

        for (int i = 0; i < amountY; i++) {
            draw(indexController, symbol);
            indexController += 4;
            draw(indexController, symbol);
            indexController += 6;
        }

        for (int i = 0; i < amountX; i++) {
            draw(indexController + i, symbol);
        }

    }

}
