package SimpleDrawingBoard_Task2;

class Canvas {
    private int width;
    private int height;
    private final String verticalLine;
    private final String horizontalLine;
    private final String space;
    private final String lineBreak;
    private String symbol;
    private String canvasDraw;
    private String fill;
    int numberSymbol;

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
        canvasDraw = "";

        numberSymbol = width -2;
        verticalLine = "|";
        horizontalLine = "-";
        space = " ";
        lineBreak = "\n";
        symbol = "x";
        fill = " ";
    }

    public Canvas draw(int x1, int y1, int x2, int y2) {
        canvasDraw += horizontalLine.repeat(width + 2) + lineBreak;

        for (int i = 0; i < height; i++) {

            if (i == 1 || i == height - 2){
                canvasDraw += verticalLine + space + symbol.repeat(x2) + space + verticalLine + lineBreak;
            } else if (i > 1 && i < height - 1) {
                canvasDraw+=verticalLine+space+symbol+fill.repeat(numberSymbol-2)+symbol+space+verticalLine+lineBreak;
            } else {
                canvasDraw += verticalLine + space.repeat(width) + verticalLine + lineBreak;
            }

        }

        canvasDraw += horizontalLine.repeat(width + 2) + lineBreak;
        return this;
    }

    public void addFill(char fill){
         this.fill = String.valueOf(fill);
    }

    public void generateBoard(){
        canvasDraw += horizontalLine.repeat(width + 2) + lineBreak;

        for (int i = 0; i < height; i++) {
            if (i == 1 || i == height - 2){
                canvasDraw += verticalLine + space + symbol.repeat(numberSymbol) + space + verticalLine + lineBreak;
            } else if (i > 1 && i < height - 1) {
                canvasDraw+=verticalLine+space+symbol+fill.repeat(numberSymbol-2)+symbol+space+verticalLine+lineBreak;
            } else {
                canvasDraw += verticalLine + space.repeat(width) + verticalLine + lineBreak;
            }
        }

        canvasDraw += horizontalLine.repeat(width + 2) + lineBreak;
    }

    public void printBoard(){
        System.out.println(canvasDraw);
    }

    public void deleteSymbol(){
        symbol = " ";
    }
    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getCanvasDraw() {
        return canvasDraw;
    }
}
