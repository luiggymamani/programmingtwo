package SimpleDrawingBoard_Task2;

public class Main {

    public static void main(String[] args){

        /**
         * Esta clase fue la primera que hice y estaba perdido en entender el proposito de la tarea.
         *
         * El proposito de esta clase es crear board segun un alto y ancho con figuras por dentro y que esten adaptadas
         * al ancho y alto.
         *
         * Aqui algunos ejemplos:
         */

        System.out.println("-".repeat(15)+" CANVAS "+"-".repeat(15));

        Canvas canvasOne = new Canvas(4, 2);
        canvasOne.deleteSymbol();
        canvasOne.generateBoard();
        canvasOne.printBoard();

        Canvas canvasTwo = new Canvas(15, 6);
        canvasTwo.generateBoard();
        canvasTwo.printBoard();

        Canvas canvasThree = new Canvas(7, 6);
        canvasThree.addFill('o');
        canvasThree.generateBoard();
        canvasThree.printBoard();


        /**
         * Cuando entendi el proposito del problema.
         *
         * En esta clase crea un StringBuilder para poder modificar sus indeces y dar la ilucion de dibujar sobre el
         * board, hice lo que pude pero no lo complete.
         *
         * Lo unico que afecta mucho es el paramtro amountX, que debe ser 5 ya que no se adapta como el anterior.
         *
         * Algunos ejemplos de como se podria mover un dibujo:
         */

        System.out.println("-".repeat(15)+" CANVAS 2 "+"-".repeat(15));

        CanvasTwo canvasTwo1= new CanvasTwo(7, 7);
        canvasTwo1.createContour();
        canvasTwo1.generateDraw(22, 5, 2);
        canvasTwo1.printBoard();

        CanvasTwo canvasTwo2 = new CanvasTwo(7, 7);
        canvasTwo2.createContour();
        canvasTwo2.generateDraw(32, 5, 3);
        canvasTwo2.printBoard();

    }
}

