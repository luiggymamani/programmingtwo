package RoundRobin;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;
import java.util.ArrayList;

public class Server {

    public static void main(String[] args) {

        try {
            final int serverPort = 5000;
            TaskExecutor taskExecutor = new TaskExecutor(5);
            View view = new View();
            ServerSocket serverSocket = new ServerSocket(serverPort);
            ArrayList<Task> tasks;
            view.showServerStarted();

            while (true) {
                Socket socket = serverSocket.accept();
                ObjectInputStream receivedPacket = new ObjectInputStream(socket.getInputStream());
                tasks = (ArrayList<Task>) receivedPacket.readObject();
                taskExecutor.addTasks(tasks);

                if (!taskExecutor.getTaskQueue().isEmpty()) view.showTaskQueue(taskExecutor.getTaskQueue());
                while (!taskExecutor.getTaskQueue().isEmpty()) taskExecutor.executeTasks();
            }

        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }
}
