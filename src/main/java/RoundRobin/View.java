package RoundRobin;

import java.util.Queue;
import java.util.concurrent.TimeUnit;

public class View {

    public static final String YELLOW = "\u001B[33m";
    public static final String BLUE = "\u001B[34m";
    public static final String PURPLE = "\u001B[35m";
    public static final String CYAN = "\u001B[36m";
    public static final String RED_BOLD = "\033[1;31m";

    public void showServerStarted() {
        System.out.println(BLUE + "-".repeat(50) + "\n");
        System.out.println(BLUE + "\t\t\t\tServer Started");
        System.out.println("\n" + BLUE + "-".repeat(50));
    }

    public void showProcessExecution() {
        System.out.print(YELLOW + "\t*" + YELLOW);
        try { TimeUnit.SECONDS.sleep(1); }
        catch (InterruptedException e) { throw new RuntimeException(e); }
    }

    public void showTaskStatus(Task task) {
        System.out.print(PURPLE + "\n\t"+ task.getNameTask() +
                CYAN + "  |  CPU Burst: " + task.getCpuBurst() + CYAN);
        if (task.isRunning()) System.out.println(YELLOW + "  |  Time Started: " + task.getTimeStarted() + YELLOW);
        else System.out.println(BLUE + "  |  Finished Time: " + task.getFinishedTime() + BLUE);

    }

    public void showTotalTime(int time) {
        System.out.println("\n" + RED_BOLD + "-".repeat(40) + "\n");
        System.out.println(" ".repeat(10) + "Total Time  :  " + time);
        System.out.println("\n" + RED_BOLD + "-".repeat(40));
    }

    public void showTaskQueue(Queue<Task> taskQueue) {
        int numberTask = 1;

        System.out.println("\n"+ YELLOW + "-".repeat(78) + "\n");
        for (Task task : taskQueue) {
            System.out.print(" ".repeat(3) + PURPLE + numberTask + ". " + task.getNameTask() + PURPLE);
            numberTask++;
        }
        System.out.println("\n\n"+ YELLOW + "-".repeat(78) + "\n");
    }

}
