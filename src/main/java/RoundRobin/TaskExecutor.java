package RoundRobin;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

public class TaskExecutor {

    int quantum;
    int time;
    Queue<Task> taskQueue;
    View view;
    Task runningTask;

    public TaskExecutor(int quantum) {
        this.quantum = quantum;
        time = -1;
        runningTask = null;
        taskQueue = new ArrayDeque<>();
        view = new View();
    }

    public void addTask(Task task) {
        taskQueue.add(task);
    }

    public void addTasks(ArrayList<Task> tasks) {
        taskQueue.addAll(tasks);
    }

    public void executeTasks() {
        runningTask = taskQueue.poll();
        assert runningTask != null;
        runningTask.setTimeStarted(time + 1);
        time++;
        runningTask.setRunning(true);
        view.showTaskStatus(runningTask);

        while (quantum > 0) {
            runningTask.setCpuBurst(runningTask.getCpuBurst() - 1);
            quantum--;
            time++;
            view.showProcessExecution();
            if (runningTask.getCpuBurst() == 0) break;
        }

        runningTask.setFinishedTime(time);
        runningTask.setRunning(false);
        view.showTaskStatus(runningTask);
        quantum = 5;
        if (runningTask.getCpuBurst() > 0) taskQueue.add(runningTask);
        if (taskQueue.isEmpty()) view.showTotalTime(time);
    }

    public Queue<Task> getTaskQueue() {
        return taskQueue;
    }

    public Task getRunningTask() {
        return runningTask;
    }

    public int getTime() {
        return time;
    }
}
