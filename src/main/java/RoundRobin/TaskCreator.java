package RoundRobin;

import java.util.ArrayList;
import java.util.Random;

public class TaskCreator {

    ArrayList<Task> taskList;
    Random random;

    public TaskCreator() {
        taskList = null;
        random = new Random();
    }

    private void createTaskList() {
        ArrayList<Task> taskList = new ArrayList<>();
        taskList.add(new Task("Notion", 9));
        taskList.add(new Task("Teams", 4));
        taskList.add(new Task("Google Chrome", 10));
        taskList.add(new Task("intelliJ", 13));
        taskList.add(new Task("SublimeText", 6));
        taskList.add(new Task("VSCode", 8));
        taskList.add(new Task("Terminal", 2));
        taskList.add(new Task("Firefox", 7));
        taskList.add(new Task("Office", 7));
        taskList.add(new Task("VLC", 5));
        this.taskList = taskList;
    }

    public Task generateTask() {
        createTaskList();
        int randomIndex = random.nextInt(taskList.size());
        return taskList.get(randomIndex);
    }

    public ArrayList<Task> generateTasks(int numberTasks) {
        ArrayList<Task> tasks = new ArrayList<>();
        Task task;
        while (numberTasks > 0) {
            task = generateTask();
            if (!containsTask(tasks, task)) {
                tasks.add(task);
                numberTasks--;
            }
        }
        return tasks;
    }

    private boolean containsTask(ArrayList<Task> tasks, Task task) {
        String name;
        for (Task taskToCompare : tasks) {
            name = taskToCompare.getNameTask();
            if (name.equals(task.getNameTask())) return true;
        }
        return false;
    }

}
