package RoundRobin;

import java.io.Serializable;

public class Task implements Serializable {

    String nameTask;
    int cpuBurst;
    int timeStarted;
    int finishedTime;
    boolean isRunning;

    public Task(String nameTask, int cpuBurst) {
        this.nameTask = nameTask;
        this.cpuBurst = cpuBurst;
        timeStarted = 0;
        finishedTime = 0;
        isRunning = false;
    }

    public String getNameTask() {
        return nameTask;
    }

    public int getCpuBurst() {
        return cpuBurst;
    }

    public void setCpuBurst(int cpuBurst) {
        this.cpuBurst = cpuBurst;
    }

    public int getTimeStarted() {
        return timeStarted;
    }

    public void setTimeStarted(int timeStarted) {
        this.timeStarted = timeStarted;
    }

    public int getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime(int finishedTime) {
        this.finishedTime = finishedTime;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }
}
