package RoundRobin;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.ArrayList;

public class Client {

    public static void main(String[] args) {
        try {
            TaskCreator taskCreator = new TaskCreator();
            Socket socket = new Socket("localhost", 5000);
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());

            ArrayList<Task> tasks = taskCreator.generateTasks(5);
            output.writeObject(tasks);
            output.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
