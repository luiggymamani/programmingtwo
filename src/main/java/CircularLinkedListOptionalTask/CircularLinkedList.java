package CircularLinkedListOptionalTask;

public class CircularLinkedList<T> implements List<T> {
    private Node<T> head;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean add(T data) {
        Node<T> newNode = new Node<>(data);
        if (isEmpty()){
            head = newNode;
            head.setNext(head);
        } else {
            Node<T> lastNode = findLastNode();
            lastNode.setNext(newNode);
            newNode.setNext(head);
        }
        size++;
        return contains(data);
    }

    public Node<T> findLastNode(){
        Node<T> lastNode = null;
        for (int index = 0; index < size; index++) {
            lastNode = getNode(index);
            if (lastNode.getNext().equals(head)){
                return lastNode;
            }
        }
        return lastNode;
    }

    public String buildList(){
        StringBuilder list = new StringBuilder("{");
        for (int index = 0; index < size; index++) {
            if (index == size -1){
                list.append(getNode(index).getData());
            } else {
                list.append(getNode(index).getData()).append(", ");
            }
        }
        list.append("}");
        return list.toString();
    }

    public Node<T> getNode(int index){
        Node<T> node = head;
        for (int indexToCompare = 0; indexToCompare < index; indexToCompare++) {
            node = node.getNext();
        }
        return node;
    }

    @Override
    public boolean remove(T data) {
        Node<T> anterior = head;
        boolean isRemoved = false;

        for (int index = 0; index < size; index ++) {
            if (get(index).equals(data)) {
                if (index == 0){
                    findLastNode().setNext(head.getNext());
                    head = getNode(index).getNext();
                    isRemoved = true;
                } else {
                    anterior.setNext(getNode(index).getNext());
                    isRemoved = true;
                }
                size--;
            }
            anterior = getNode(index);
        }


        return isRemoved;
    }

    public boolean contains(T data){
        boolean isContained = false;
        for (int index = 0; index < size; index++) {
            if (getNode(index).getData().equals(data)){
                isContained = true;
            }
        }
        return isContained;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());
            return node.getData();
        }
    }

    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }

    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }

}
