package CircularLinkedListOptionalTask;

public class Main {

    public static void main(String[] args) {
        CircularLinkedList<String> circularLinkedList = new CircularLinkedList<>();
        circularLinkedList.add("String 1");
        circularLinkedList.add("String 2");
        circularLinkedList.add("String 3");
        circularLinkedList.add("String 4");
        circularLinkedList.add("String 5");
        circularLinkedList.add("String 6");

        System.out.println("\n\tElements added   ----->   " + circularLinkedList.buildList());
        circularLinkedList.remove("String 6");
        System.out.println("\n\tRemove 'String 6'   ----->   " + circularLinkedList.buildList());
        System.out.println("\tHead   ----->   " + circularLinkedList.findLastNode().getNext().getData());
        circularLinkedList.remove("String 1");
        System.out.println("\n\tRemove 'String 1'   ----->   " + circularLinkedList.buildList());
        System.out.println("\tHead   ----->   " + circularLinkedList.findLastNode().getNext().getData());
        circularLinkedList.remove("String 4");
        System.out.println("\n\tRemove 'String 4'   ----->   " + circularLinkedList.buildList());
        System.out.println("\tHead   ----->   " + circularLinkedList.findLastNode().getNext().getData());
        circularLinkedList.remove("String 5");
        System.out.println("\n\tRemove 'String 5'   ----->   " + circularLinkedList.buildList());
        System.out.println("\tHead   ----->   " + circularLinkedList.findLastNode().getNext().getData());

    }
}
