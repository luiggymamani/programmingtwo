package ListGenericos_Task11;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JULinkedList<T> implements List<T> {

    Nodo<T> mainNode;
    Nodo<T> lastNode;

    @Override
    public int size() {
        int size = 0;
        Nodo<T> node = mainNode;

        while (node != null){
            node = node.getNextNode();
            size ++;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new JUIterator<>(mainNode);
    }

    @Override
    public boolean add(T t) {
        Nodo<T> newNode = new Nodo<>(t);;

        if (mainNode == null){
            mainNode = newNode;
            lastNode = newNode;
        } else {
            lastNode.nextNode = newNode;
            lastNode = newNode;
        }
        return contains(t);
    }

    @Override
    public boolean remove(Object object) {
        Nodo<T> nodeToCompare = mainNode;
        Nodo<T> auxNode = mainNode;

        while (nodeToCompare != null) {
            if (nodeToCompare.getNodeData() == object) {
                if (nodeToCompare == mainNode)
                    mainNode = nodeToCompare.getNextNode();
                else {
                    auxNode.setNextNode(nodeToCompare.getNextNode());
                }
                break;
            }
            auxNode = nodeToCompare;
            nodeToCompare = nodeToCompare.getNextNode();
        }

        return !contains(object);
    }

    @Override
    public void clear() {
        mainNode = null;
    }

    @Override
    public T get(int index) {
        Nodo<T> nodeToCompare = mainNode;
        for (int count = 0; count < index; count++) {
            nodeToCompare = nodeToCompare.getNextNode();
        };
        return nodeToCompare.getNodeData();
    }

    @Override
    public T remove(int index) {
        Nodo<T> nodeToCompare = mainNode;
        Nodo<T> auxNode = mainNode;

        for (int indexToCompare = 0; indexToCompare < size(); indexToCompare++) {
            if (indexToCompare == index) {
                if (index == 0)
                    mainNode = nodeToCompare.getNextNode();
                else {
                    auxNode.setNextNode(nodeToCompare.getNextNode());
                }
                break;
            }
            auxNode = nodeToCompare;
            nodeToCompare = nodeToCompare.getNextNode();
        }

        return getNode(index).getNodeData();
    }

    @Override
    public boolean contains(Object object) {
        boolean isContained = false;
        Nodo<T> nodeToCompare = mainNode;
        Object objectToCompare;

        while (nodeToCompare != null){
            objectToCompare = nodeToCompare.getNodeData();
            if (objectToCompare.equals(object)){
                isContained = true;
            }
            nodeToCompare = nodeToCompare.getNextNode();
        }

        return isContained;
    }

    @Override
    public T set(int index, T t) {
        if (index >= 0 && index < size()){
            getNode(index).setNodeData(t);
        }
        return t;
    }

    @Override
    public int indexOf(Object object) {
        T dataNode;
        int indexObject = 0;

        for (int index = 0; index < size(); index++) {
            dataNode = getNode(index).getNodeData();
            if (dataNode.equals(object)){
                indexObject = index;
                break;
            }
        }
        return indexObject;
    }

    private Nodo<T> getNode(int index) {
        Nodo<T> node = mainNode;
        for (int count = 0; count < index; count++) {
            node = node.getNextNode();
        }
        return node;
    }

    @Override
    public int lastIndexOf(Object object) {
        for (int index = size() - 1; index > 0; index--) {
            if ((getNode(index).getNodeData()).equals(object)){
                return index;
            }
        }
        return 0;
    }

    public String buildList(){
        Nodo<T> node = mainNode;
        StringBuilder list = new StringBuilder("{");

        while (node != null){
            if (node.getNextNode() == null){
                list.append(node.getNodeData());
                node = node.getNextNode();
            }
            else{
                list.append(node.getNodeData()).append(", ");
                node = node.getNextNode();
            }
        }
        return (list.append("}")).toString();
    }

    @Override
    public void add(int index, T t) {
        System.out.println("Optional Method");
    }

    @Override
    public Object[] toArray() {
        System.out.println("Optional Method");
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] t1s) {
        System.out.println("Optional Method");
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        System.out.println("Optional Method");
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        // unspecified method to implement
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        // unspecified method to implement
        return false;
    }

    @Override
    public boolean addAll(int i, Collection<? extends T> collection) {
        // unspecified method to implement
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        // unspecified method to implement
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        // unspecified method to implement
        return false;
    }

    @Override
    public ListIterator<T> listIterator() {
        // unspecified method to implement
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int i) {
        // unspecified method to implement
        return null;
    }
}
