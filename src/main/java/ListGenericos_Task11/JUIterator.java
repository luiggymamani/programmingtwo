package ListGenericos_Task11;

import java.util.Iterator;

public class JUIterator<T> implements Iterator<T> {

    Nodo<T> mainNode;
    int iterationCounter;

    public JUIterator(Nodo<T> mainNode) {
        this.mainNode = mainNode;
        iterationCounter = 0;
    }

    @Override
    public boolean hasNext() {
        return mainNode.getNextNode() != null;
    }

    @Override
    public T next() {
        T nodeData = null;
        if (iterationCounter == 0){
            nodeData = mainNode.getNodeData();
            iterationCounter++;
        } else if (hasNext()) {
            nodeData = mainNode.getNextNode().getNodeData();
            mainNode = mainNode.getNextNode();
            iterationCounter++;
        }
        return nodeData;
    }
}
