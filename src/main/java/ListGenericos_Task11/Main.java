package ListGenericos_Task11;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> list = new JULinkedList<>();

        list.add("String 1");
        list.add("String 2");
        list.add("String 3");
        list.add("String 4");
        list.add("String 5");
        list.add("String 4");

        System.out.println("\nElement added to List  --------->  " + ((JULinkedList<String>) list).buildList());
        System.out.println("\n\tSize  ---------> " + list.size());
        System.out.println("\tIs Empty  --------->  " + list.isEmpty());
        System.out.println("\t-----------Iterator-----------");
        Iterator<String> iterator = list.iterator();
        do {
            System.out.println("\t" + iterator.next());
        } while (iterator.hasNext());

        list.remove("String 2");
        System.out.println("\n\tRemover object 'String 2'  --------->   " + ((JULinkedList<String>) list).buildList());
        System.out.println("\tIndex 1  --------->   " + list.get(1));
        System.out.println("\tContains 'String 5'  --------->   " + list.contains("String 5"));
        list.set(1, "String 100");
        System.out.println("\tSet object 'String 100' on index 1  --------->   " + ((JULinkedList<String>) list).buildList());
        System.out.println("\tIndex Of 'String 4'  --------->  " + list.indexOf("String 4"));
        System.out.println("\tLast Index Of 'String 4'  --------->  " + list.lastIndexOf("String 4"));
        list.remove(0);
        System.out.println("\tRemove element from 'index 0'  --------->  " + list.contains("String 5"));
        list.clear();
        System.out.println("\tDeleted list  --------->  " + ((JULinkedList<String>) list).buildList());
        System.out.println("\tIs Empty  --------->  " + list.isEmpty());

    }

}
