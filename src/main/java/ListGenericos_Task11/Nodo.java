package ListGenericos_Task11;

class Nodo<T> {

    T nodeData;
    Nodo<T> nextNode;

    public Nodo(T data) {
        this.nodeData = data;
        nextNode = null;
    }

    public T getNodeData() {
        return nodeData;
    }

    public Nodo<T> getNextNode() {
        return nextNode;
    }

    public void setNextNode(Nodo<T> nextNode) {
        this.nextNode = nextNode;
    }

    public void setNodeData(T nodeData) {
        this.nodeData = nodeData;
    }

    public String toString() {
        return "{" + nodeData + ", sig->" + nextNode + "}";
    }

}
