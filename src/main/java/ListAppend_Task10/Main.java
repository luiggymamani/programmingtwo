package ListAppend_Task10;

public class Main {

    public static void main(String[] args) {

        Nodo listA = new Nodo(1);
        listA.setNextNode(new Nodo(2));
        listA.getNextNode().setNextNode(new Nodo(3));

        Nodo listB = new Nodo(4);
        listB.setNextNode(new Nodo(5));
        listB.getNextNode().setNextNode(new Nodo(6));

        Nodo listAppend = Nodo.append(listA, listB);
        System.out.println(Nodo.generateViwList(listAppend));


    }
}
