package ListAppend_Task10;

public class Nodo {

    int nodeData;
    Nodo nextNode;

    public Nodo(int data) {
        this.nodeData = data;
        nextNode = null;
    }

    public static Nodo append(Nodo listA, Nodo listB) {
        return appendOnlyList(appendOnlyList(null, listA), listB);
    }

    private static Nodo appendOnlyList(Nodo listAppend, Nodo list){
        Nodo lastNode = null;
        Nodo newNode;
        int data;

        if (listAppend != null) lastNode = listAppend.getNode(Nodo.calculateLength(listAppend) - 1);

        if (list != null){
            for (int index = 0; index < calculateLength(list); index++) {
                data = getNth(list, index);
                newNode = new Nodo(data);

                if (listAppend == null){
                    listAppend = newNode;
                    lastNode = newNode;
                } else {
                    lastNode.nextNode = newNode;
                    lastNode = newNode;
                }
            }
        }

        return listAppend;
    }

    public static int getNth(Nodo node, int index){
        int nodeData = node.getNodeData();

        for (int count = 0; count < index; count++) {
            node = node.getNextNode();
            nodeData = node.getNodeData();
        }

        return nodeData;
    }

    public static int calculateLength(Nodo mainNode){
        int length = 0;
        while (mainNode != null){
            mainNode = mainNode.getNextNode();
            length ++;
        }
        return length;
    }

    public Nodo getNode(int index) {
        Nodo node = this;
        for (int count = 0; count < index; count++) {
            node = node.getNextNode();
        }
        return node;
    }

    public static String generateViwList(Nodo list){
        String viewList = "";
        if (list != null){
            for (int index = 0; index < calculateLength(list); index++) {
                viewList += Nodo.getNth(list, index) + " -> ";
            }
        }
        return viewList;
    }

    public int getNodeData() {
        return nodeData;
    }

    public Nodo getNextNode() {
        return nextNode;
    }

    public void setNextNode(Nodo nextNode) {
        this.nextNode = nextNode;
    }

}
