package Recursividad_Task15;

public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo - done
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour - done
        LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue() - done
        // 1.- sacar screenshot de la exception - done
        // 2.- a continuacion describir el problema: - done

        /**
         * Descripcion:
         * El metodo esta aplicando recursividad, es decir, se esta llamando a si mismo para 'crear'
         * un ciclo o bucle, lo malo es que no tiene una condicional que lo hago parar en un punto o caso especifico,
         * es decir, se ejecutara como un bucle infinito.
         */

        // 3.- volver a comentar la llmanda a: Infinito.whileTrue() - done

        //Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasando un numero entero positivo
        // 1.- sacar screenshot de la salida del metodo - done
        // 2.- a continuacion describir como funciona este metodo: - done

        /**
         * Descripcion:
         * 1. Ejecutar la instruccion: imprimir el numero que se le paso como parametro
         * 2. Entra a la condicional: si el numero es mayor a cero volvera a llamarse (aplicando recursividad)
         * pero el parametro que se le pasara sera un numero menos que el anterior, lo que causara que en algun momento
         * el metodo dejara de llamarse por no cumplir la condicional.
         *
         * En resumen, se esta aplicando recursividad para simular un bucle, donde se tiene una instruccion a cumplir
         * y una condicional que controla las veces que la instruccion de este bucle se ejecute.
         */

        Contador.contarHasta(5);

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));


    }
}

