package ImplementInterface_Task7;

import java.util.*;

public class JUArrayList implements List<Integer>, Iterator<Integer>{

    protected int[] arrayList;
    protected int count;
    int elementIndex;

    public JUArrayList() {
        arrayList = new int[0];
        count = 0;
        elementIndex = 0;
    }

    @Override
    public int size() {
        return arrayList.length;
    }

    @Override
    public boolean isEmpty() {
        return arrayList.length == 0;
    }

    @Override
    public Iterator<Integer> iterator() {
        while (hasNext()){
            System.out.println(next());
            elementIndex ++;
        }
        return new IntegersIterator();
    }

    @Override
    public boolean hasNext() {
        return elementIndex < arrayList.length;
    }

    @Override
    public Integer next() {
        return arrayList[elementIndex];
    }

    @Override
    public boolean add(Integer integer) {
        if (count == arrayList.length) {
            int[] arrayListCopy = new int[arrayList.length + 1];
            System.arraycopy(arrayList, 0, arrayListCopy, 0, arrayList.length);
            arrayList = arrayListCopy;
        }
        arrayList[count] = integer;
        count++;
        return contains(integer);
    }

    /**
     * According to the Oracle documentation: "Removes the first occurrence of the specified
     * element from this list, if it is present"
     *
     * That is why there is a break in the conditional, but if this break is removed,
     * all the objects in the list will be eliminated and not just the first one.
     *
     * @param object is the object to be removed.
     * @return checks if the object to be deleted is the same as the one that was deleted.
     */
    @Override
    public boolean remove(Object object) {
        Integer numberToRemove = (Integer)object;
        int numberToCompare;

        if (contains(object)){
            for (int index = 0; index < arrayList.length; index++) {
                numberToCompare = arrayList[index];
                if (Objects.equals(numberToRemove, numberToCompare)){
                    numberToRemove = remove(index);
                    break;
                }
            }
        }
        return numberToRemove.equals(object);
    }

    @Override
    public void clear() {
        arrayList = new int[0];
    }

    @Override
    public Integer get(int index) {
        return arrayList[index];
    }

    @Override
    public Integer remove(int indexToRemove) {
        int deletedNumber;

        if (indexToRemove < arrayList.length && indexToRemove >= 0){
            deletedNumber = arrayList[indexToRemove];
            int[] arrayListCopy = new int[arrayList.length - 1];

            for (int index = 0; index < arrayList.length; index++) {
                if (index < indexToRemove){
                    arrayListCopy[index] = arrayList[index];
                } else if (index > indexToRemove) {
                    arrayListCopy[index - 1] = arrayList[index];
                }
            }
            arrayList = arrayListCopy;
        } else {
            return null;
        }
        return deletedNumber;
    }

    @Override
    public boolean contains(Object object) {
        boolean isExisting = false;
        for (Integer integerToCompare : arrayList) {
            if (Objects.equals(object, integerToCompare)) {
                isExisting = true;
                break;
            }
        }
        return isExisting;
    }

    @Override
    public Integer set(int index, Integer integer) {
        if (index >= 0 && index < arrayList.length){
            arrayList[index] = integer;
        }
        return integer;
    }

    @Override
    public void add(int index, Integer integer) {
        if (index >= 0 && index < arrayList.length){
            int[] arrayListCopy = new int[arrayList.length + 1];

            for (int indexToCompare = 0; indexToCompare < arrayList.length + 1; indexToCompare++) {
                if (indexToCompare < index){
                    arrayListCopy[indexToCompare] = arrayList[indexToCompare];
                } else if (indexToCompare == index) {
                    arrayListCopy[indexToCompare] = integer;
                } else {
                    arrayListCopy[indexToCompare] = arrayList[indexToCompare - 1];
                }
            }
            arrayList = arrayListCopy;
        }
    }

    @Override
    public Object[] toArray() {
        System.out.println("optional method");
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        System.out.println("optional method");
        return null;
    }

    public int[] getArrayList() {
        return arrayList;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        // unlisted method not implemented
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> collection) {
        // unlisted method not implemented
        return false;
    }

    @Override
    public boolean addAll(int i, Collection<? extends Integer> collection) {
        // unlisted method not implemented
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        // unlisted method not implemented
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        // unlisted method not implemented
        return false;
    }

    @Override
    public int indexOf(Object o) {
        // unlisted method not implemented
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        // unlisted method not implemented
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        // unlisted method not implemented
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int i) {
        // unlisted method not implemented
        return null;
    }

    @Override
    public List<Integer> subList(int i, int i1) {
        // unlisted method not implemented
        return null;
    }


}
