package ImplementInterface_Task7;

import java.util.Iterator;

public class IntegersIterator extends JUArrayList implements Iterator<Integer> {

    @Override
    public boolean hasNext() {
        return elementIndex < arrayList.length;
    }

    @Override
    public Integer next() {
        return arrayList[elementIndex];
    }
}
