package ImplementInterface_Task7;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        JUArrayList juArrayList = new JUArrayList();
        System.out.println("-----add-----");
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);

        System.out.println(Arrays.toString(juArrayList.getArrayList()));
        System.out.println("-----size-----");
        System.out.println(juArrayList.size());
        System.out.println("-----empty-----");
        System.out.println(juArrayList.isEmpty());
        System.out.println("-----Iterator-----");
        juArrayList.iterator();
        System.out.println("-----remove with object-----");
        juArrayList.remove((Object)30);
        System.out.println(Arrays.toString(juArrayList.getArrayList()));
        System.out.println("-----get-----");
        System.out.println(juArrayList.get(4));
        System.out.println("-----remove with index-----");
        juArrayList.remove(4);
        System.out.println(Arrays.toString(juArrayList.getArrayList()));
        System.out.println("-----set-----");
        juArrayList.set(2, 1000);
        System.out.println(Arrays.toString(juArrayList.getArrayList()));
        System.out.println("-----contains-----");
        System.out.println(juArrayList.contains(1000));
        System.out.println("-----add with index-----");
        juArrayList.add(2, 500);
        System.out.println(Arrays.toString(juArrayList.getArrayList()));

        System.out.println("-----result--------");
        System.out.println(Arrays.toString(juArrayList.getArrayList()));

    }
}
