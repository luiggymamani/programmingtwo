package GameArray_Task6;

public class ViewOfGame {

    public ViewOfGame() {
    }

    public void printMap(String[][] arrayMap){
        for (String[] row : arrayMap) {
            for (String column : row) {
                System.out.print(column);
            }
            System.out.println();
        }
        System.out.println();
    }

    public void printWelcomeMessage(){
        System.out.println("-".repeat(65));
        System.out.println(" ".repeat(24) + "WELCOME TO MY GAME");
        System.out.println("-".repeat(65));
    }

    public void printInstructions(){
        System.out.println("Instructions To Move");
        System.out.println("\tMove Up press   ---> w" + " ".repeat(5) + "Move Down press  ---> s");
        System.out.println("\tMove Left press ---> a" + " ".repeat(5) +"Move Right press ---> d\n");
        System.out.println("The game ends when you reach the bottom right corner of the map.\n");
    }

    public void printInvalidActionMessage(){
        System.out.println("Invalid Address");
    }

    public void printWinnerMessage(){
        System.out.println("CONGRATULATIONS!!! " + "\n");
    }

    public void printStatistics(String namePlayer, int totalMovements){
        System.out.println("\tPlayer : " + namePlayer);
        System.out.println("\tMovements : " + totalMovements);
    }

    public void printDirectionMessage(){
        System.out.print("Direction ---> " );
    }
}
