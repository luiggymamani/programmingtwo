package GameArray_Task6;

public class Game {

    Player player;
    Map map;
    PlayerMoves playerMoves;
    ViewOfGame viewOfGame;

    public Game(Player player, Map map) {
        this.player = player;
        this.map = map;

        playerMoves = new PlayerMoves(player, map);
        viewOfGame = new ViewOfGame();
    }

    public void runGame(){
        boolean isRunningGame = true;
        int[] playerPosition;
        int totalMovements = 0;

        viewOfGame.printWelcomeMessage();
        viewOfGame.printInstructions();
        map.generatedMap(player);
        viewOfGame.printMap(map.getArrayMap());

        while (isRunningGame){
            playerMoves.movePlayer(viewOfGame);
            viewOfGame.printMap(map.getArrayMap());
            playerPosition = playerMoves.findPlayer();
            totalMovements ++;

            if (map.checkWinningPosition(playerPosition[0], playerPosition[1])){
                isRunningGame = false;
                viewOfGame.printWinnerMessage();
                viewOfGame.printStatistics(player.getNamePlayer(), totalMovements);
            }
        }
    }

}

