package GameArray_Task6;

public class Player {

    private String namePlayer;
    private String playerSymbol;

    public Player(String namePlayer, String playerSymbol) {
        this.namePlayer = namePlayer;
        this.playerSymbol = playerSymbol;
    }

    public String getPlayerSymbol() {
        return playerSymbol;
    }

    public String getNamePlayer() {
        return namePlayer;
    }
}
