package GameArray_Task6;

import java.util.Arrays;

public class Map {

    private String[][] arrayMap;
    private int numberOfObstacles;
    private final String OBSTACLE;
    private final String SPACE;

    public Map(int lengthMap, int numberOfObstacles) {
        this.numberOfObstacles = numberOfObstacles;
        arrayMap = new String[lengthMap][];

        OBSTACLE = " # ";
        SPACE = " . ";
    }

    public void fillTheMap(){
        String[] subArray;
        for (int mainIndex = 0; mainIndex < arrayMap.length; mainIndex++) {
            subArray = new String[arrayMap.length];
            arrayMap[mainIndex] = subArray;
            Arrays.fill(arrayMap[mainIndex], SPACE);
        }
    }

    public void insertPlayer(int axisX, int axisY, Player player){
        arrayMap[axisY][axisX] = player.getPlayerSymbol();
    }

    public void insertObstacle(int axisX, int axisY){
        arrayMap[axisY][axisX] = OBSTACLE;
    }

    public boolean checkObstacle(int axisX, int axisY){
        return !arrayMap[axisY][axisX].equals(OBSTACLE);
    }

    public boolean checkPlayer(int axisX, int axisY, Player player){
        return arrayMap[axisY][axisX].equals(player.getPlayerSymbol());
    }

    public boolean checkLimits(int axisX, int axisY){
        return axisX >= 0 && axisY >= 0 && axisX != arrayMap.length && axisY != arrayMap.length;
    }

    public boolean checkWinningPosition(int axisXPlayer, int axisYPlayer){
        return arrayMap[axisYPlayer][axisXPlayer].equals(arrayMap[arrayMap.length - 1][arrayMap.length - 1]);
    }

    private void generateRandomPlayer(Player player){
        int axisX, axisY;
        boolean isDoNotGeneratedPlayer = true;

        while (isDoNotGeneratedPlayer){
            axisX = (int)(Math.random() * arrayMap.length);
            axisY = (int)(Math.random() * arrayMap.length);

            if (checkLimits(axisX, axisY) || checkObstacle(axisX, axisY) || !checkWinningPosition(axisX, axisY)) {
                insertPlayer(axisX, axisY, player);
                isDoNotGeneratedPlayer = false;
            }
        }
    }

    private void generateRandomObstacles(){
        int axisX, axisY;
        while (numberOfObstacles > 0){
            axisX = (int)(Math.random()* arrayMap.length);
            axisY = (int)(Math.random()* arrayMap.length);

            if (checkLimits(axisX, axisY) || checkObstacle(axisX, axisY) || !checkWinningPosition(axisX, axisY)){
                insertObstacle(axisX, axisY);
            }
            numberOfObstacles --;
        }
    }

    public void generatedMap(Player player){
        fillTheMap();
        generateRandomObstacles();
        generateRandomPlayer(player);
    }

    public String[][] getArrayMap() {
        return arrayMap;
    }

    public String getSPACE() {
        return SPACE;
    }

    public void setArrayMap(String[][] arrayMap) {
        this.arrayMap = arrayMap;
    }
}
