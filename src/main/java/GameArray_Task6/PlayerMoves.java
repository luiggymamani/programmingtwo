package GameArray_Task6;

import java.util.Scanner;

public class PlayerMoves {

    Scanner scanner;
    Player player;
    Map map;

    public PlayerMoves(Player player, Map map) {
        this.player = player;
        this.map = map;
        scanner = new Scanner(System.in);
    }


    public int[] findPlayer(){
        int [] positionPlayer = new int[2];

        for (int axisX = 0; axisX < map.getArrayMap().length; axisX++) {
            for (int axisY = 0; axisY < map.getArrayMap()[axisX].length; axisY++) {
                if (map.getArrayMap()[axisX][axisY].equals(player.getPlayerSymbol())){
                    positionPlayer[0] = axisY;
                    positionPlayer[1] = axisX;
                }
            }
        }

        return positionPlayer;
    }

    public void movePlayer(String direction, ViewOfGame viewOfGame){
        int[] positionPlayer = findPlayer();
        int axisX = positionPlayer[0];
        int axisY = positionPlayer[1];

        switch (direction) {
            case "w":
                movePlayerUp(axisX, axisY);
                break;
            case "s":
                movePlayerDown(axisX, axisY);
                break;
            case "a":
                movePlayerLeft(axisX, axisY);
                break;
            case "d":
                movePlayerRight(axisX, axisY);
                break;
            default:
                viewOfGame.printInvalidActionMessage();
        }
    }

    public void movePlayer(ViewOfGame viewOfGame){
        viewOfGame.printDirectionMessage();
        String direction = scanner.nextLine();
        movePlayer(direction, viewOfGame);
    }

    public void movePlayerRight(int axisXPlayer, int axisYPlayer){
        String[][] arrayMap = map.getArrayMap();
        if (map.checkLimits(axisXPlayer + 1, axisYPlayer) && map.checkObstacle(axisXPlayer + 1, axisYPlayer)) {
            arrayMap[axisYPlayer][axisXPlayer] = map.getSPACE();
            arrayMap[axisYPlayer][axisXPlayer + 1] = player.getPlayerSymbol();
        }
        map.setArrayMap(arrayMap);
    }

    public void movePlayerLeft(int axisXPlayer, int axisYPlayer){
        String[][] arrayMap = map.getArrayMap();
        if (map.checkLimits(axisXPlayer - 1, axisYPlayer) && map.checkObstacle(axisXPlayer - 1, axisYPlayer)) {
            arrayMap[axisYPlayer][axisXPlayer] = map.getSPACE();
            arrayMap[axisYPlayer][axisXPlayer - 1] = player.getPlayerSymbol();
        }
        map.setArrayMap(arrayMap);
    }

    public void movePlayerUp(int axisXPlayer, int axisYPlayer){
        String[][] arrayMap = map.getArrayMap();
        if (map.checkLimits(axisXPlayer, axisYPlayer - 1) && map.checkObstacle(axisXPlayer, axisYPlayer - 1)) {
            arrayMap[axisYPlayer][axisXPlayer] = map.getSPACE();
            arrayMap[axisYPlayer - 1][axisXPlayer] = player.getPlayerSymbol();
        }
        map.setArrayMap(arrayMap);
    }

    public void movePlayerDown(int axisXPlayer, int axisYPlayer){
        String[][] arrayMap = map.getArrayMap();
        if (map.checkLimits(axisXPlayer, axisYPlayer + 1) && map.checkObstacle(axisXPlayer, axisYPlayer + 1)) {
            arrayMap[axisYPlayer][axisXPlayer] = map.getSPACE();
            arrayMap[axisYPlayer + 1][axisXPlayer] = player.getPlayerSymbol();
        }
        map.setArrayMap(arrayMap);
    }
}
