package RotateArray_task5;

import java.util.Arrays;

public class RotateLogic {

    public RotateLogic() {}

    public int[] rotate (int[] array, int numberRotations){

        if (numberRotations >= 0){
            for (int i = 0; i < numberRotations; i++) {
                array = positiveRotationOneTime(array);
            }
        } if (numberRotations < 0){
            numberRotations = numberRotations * -1;
            for (int i = 0; i < numberRotations; i++) {
                array = negativeRotationOneTime(array);
            }
        }
        return array;
    }

    private int[] positiveRotationOneTime (int[] array){
        int number;
        int newIndex;

        int[] moldArray = new int[array.length];
        moldArray[0] = array[array.length - 1];

        for (int index = 0; index < array.length; index ++) {
            number = array[index];
            newIndex = index + 1;

            if (newIndex == array.length){
                break;
            } else {
                moldArray[newIndex] = number;
            }

        }
        array = moldArray;
        return array;
    }

    private int[] negativeRotationOneTime (int[] array){
        int number;
        int newIndex;

        int[] moldArray = new int[array.length];
        moldArray[array.length - 1] = array[0];

        for (int index = array.length - 1 ; index > 0; index --) {
            number = array[index];
            newIndex = index - 1;

            if (newIndex < 0){
                break;
            } else {
                moldArray[newIndex] = number;
            }

        }
        array = moldArray;
        return array;
    }

    public void showArray(int[] array){
        System.out.println(Arrays.toString(array));
    }

}
