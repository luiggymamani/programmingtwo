package RotateArray_task5;

public class Main {

    public static void main(String[] args){

        int[] array = {1, 2, 3, 4, 5};
        int[] newArray;

        RotateLogic rotateLogic = new RotateLogic();

        newArray = rotateLogic.rotate(array, 12478);
        rotateLogic.showArray(newArray);

        newArray = rotateLogic.rotate(array, -12478);
        rotateLogic.showArray(newArray);

        newArray = rotateLogic.rotate(array, 0);
        rotateLogic.showArray(newArray);
    }
}
