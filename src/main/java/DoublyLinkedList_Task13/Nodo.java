package DoublyLinkedList_Task13;

public class Nodo<T> {

    private T nodeData;
    private Nodo<T> nextNode;
    private Nodo<T> previousNode;

    public Nodo(T data) {
        this.nodeData = data;
        nextNode = null;
        previousNode = null;
    }

    public T getNodeData() {
        return nodeData;
    }

    public Nodo<T> getNextNode() {
        return nextNode;
    }

    public void setNextNode(Nodo<T> nextNode) {
        this.nextNode = nextNode;
    }

    public Nodo<T> getPreviousNode() {
        return previousNode;
    }

    public void setPreviousNode(Nodo<T> previousNode) {
        this.previousNode = previousNode;
    }

    public String toString() {
        return nodeData + "  <==>  " + nextNode;
    }

}
