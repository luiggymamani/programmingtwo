package DoublyLinkedList_Task13;

public class DoublyLinkedList<T extends Comparable<T>> implements List<T>{

    Nodo<T> mainNode;
    Nodo<T> lastNode;

    public DoublyLinkedList() {
        mainNode = null;
        lastNode = null;
    }

    @Override
    public int size() {
        int size = 0;
        for (Nodo<T> node = mainNode; node != null; node = node.getNextNode()){
            size ++;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean add(T data) {
        Nodo<T> newNode = new Nodo<>(data);;
        if (isEmpty()){
            mainNode = newNode;
            lastNode = newNode;
        } else {
            lastNode.setNextNode(newNode);
            newNode.setPreviousNode(lastNode);
            lastNode = newNode;
        }
        return contains(data);
    }

    @Override
    public boolean remove(T object) {
        boolean itRemoved = false;
        if (contains(object)){
            for (Nodo<T> nodeToCompare = mainNode; nodeToCompare != null; nodeToCompare = nodeToCompare.getNextNode()){

                if (nodeToCompare.getNodeData() == object) {
                    if (nodeToCompare == mainNode){
                        if (nodeToCompare.getNextNode() == null) mainNode = null;
                        else {
                            nodeToCompare.getNextNode().setPreviousNode(null);
                            mainNode = nodeToCompare.getNextNode();
                        }
                    } else if (nodeToCompare.getNextNode() == null) {
                        nodeToCompare.getPreviousNode().setNextNode(null);
                        lastNode = nodeToCompare.getPreviousNode();
                    } else {
                        nodeToCompare.getPreviousNode().setNextNode(nodeToCompare.getNextNode());
                        nodeToCompare.getNextNode().setPreviousNode(nodeToCompare.getPreviousNode());
                    }
                    itRemoved = true;
                    break;
                }
            }
        }

        return itRemoved;
    }

    @Override
    public T get(int index) {
        return getNode(index).getNodeData();
    }

    @Override
    public void xchange(int firstIndex, int secondIndex) {
        if (firstIndex >= 0 && secondIndex >= 0 && firstIndex < size() && secondIndex < size()){
            T auxData = getNode(firstIndex).getNodeData();
            setNode(firstIndex, getNode(secondIndex).getNodeData());
            setNode(secondIndex, auxData);
        }
    }

    private void setNode(int index, T element) {
        Nodo<T> newNode = new Nodo<>(element);

        if (index - 1 < 0){
            newNode.setNextNode(getNode(index).getNextNode());
            getNode(index).getNextNode().setPreviousNode(newNode);
            mainNode = newNode;
        } else {
            newNode.setPreviousNode(getNode(index).getPreviousNode());
            newNode.setNextNode(getNode(index).getNextNode());
            getNode(index).getPreviousNode().setNextNode(newNode);
            if (index != size() - 1) getNode(index).getNextNode().setPreviousNode(newNode);
        }
    }

    public Nodo<T> getNode(int index){
        Nodo<T> node = mainNode;
        while (index > 0){
            node = node.getNextNode();
            index --;
        }
        return node;
    }

    private boolean contains(T object){
        boolean itContain = false;
        for (Nodo<T> nodeToCompare = mainNode; nodeToCompare != null; nodeToCompare = nodeToCompare.getNextNode()){
            if (nodeToCompare.getNodeData().equals(object)) {
                itContain = true;
                break;
            }
        }
        return itContain;
    }

    public String toString() {
        return "" + mainNode;
    }

    @Override
    public void selectionSort() {
        // Unspecified method to implement
    }

    @Override
    public void bubbleSort() {
        // Unspecified method to implement
    }

    @Override
    public void mergeSort() {
        // Unspecified method to implement
    }
}
