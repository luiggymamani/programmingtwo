package DoublyLinkedList_Task13;

public class Main {

    public static void main(String[] args) {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>();
        doublyLinkedList.add(1);
        doublyLinkedList.add(2);
        doublyLinkedList.add(3);
        doublyLinkedList.add(4);
        doublyLinkedList.add(5);

        System.out.println("\n" + doublyLinkedList);

        doublyLinkedList.remove(3);
        System.out.println("\n\t" + doublyLinkedList);

        doublyLinkedList.remove(5);
        System.out.println("\t" + doublyLinkedList);

        doublyLinkedList.remove(1);
        System.out.println("\t" + doublyLinkedList);

        doublyLinkedList.remove(2);
        System.out.println("\t" + doublyLinkedList);

        doublyLinkedList.remove(4);
        System.out.println("\t" + doublyLinkedList);
    }
}