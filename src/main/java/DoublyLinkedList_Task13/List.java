package DoublyLinkedList_Task13;

public interface List <T extends Comparable<T>>{

    int size();

    boolean isEmpty();

    boolean add(T data);

    boolean remove(T data);

    T get(int index);

    void xchange(int firstIndex, int secondIndex);

    void selectionSort();

    void bubbleSort();

    void mergeSort();
}
