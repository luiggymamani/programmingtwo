package Stack_Task16;

public class Operations {

    public int addition(int firsNumber, int secondNumber) {
        return firsNumber + secondNumber;
    }

    public int multiplication(int firsNumber, int secondNumber) {
        return firsNumber * secondNumber;
    }

    public int power(int number, int powerNumber) {
        int result;
        if(powerNumber > 0) {
            result = number * number;
            while(powerNumber > 2) {
                result = result * number;
                powerNumber --;
            }
        } else { result = 1; }
        return result;
    }

    public int factorial(int number) {
        int result = 1;
        for (int i = 1; i <= number; i++) {
            result = result * i;
        }
        return result;
    }
}