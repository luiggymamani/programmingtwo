package Stack_Task16;

import java.util.Objects;

public class Stack <T extends Comparable<T>> {

    Nodo<T> mainNode;

    public Stack() {
        mainNode = null;
    }

    public void push(T nodeData) {
        Nodo<T> newNode = new Nodo<>(nodeData);
        if (mainNode == null) mainNode = newNode;
        else { Objects.requireNonNull(getLastNode()).setNextNode(newNode); }
    }

    public T pop() {
        T data = null;
        if (!isEmpty()){
            data = getLastNode().getNodeData();
            for (Nodo<T> node = mainNode; node != null; node = node.getNextNode()){
                if (node.getNextNode() == getLastNode()) node.setNextNode(null);
                else if (getLastNode() == mainNode) mainNode = null;
            }
        }
        return data;
    }

    public Nodo<T> getLastNode() {
        for (Nodo<T> node = mainNode; node != null; node = node.getNextNode()){
            if (node.getNextNode() == null) return node;
        }
        return null;
    }

    public String buildStack(){
        StringBuilder stack = new StringBuilder("{");
        int numberElement = 1;
        for (Nodo<T> node = mainNode; node != null; node = node.getNextNode()){
            stack.append(numberElement).append(". ").append(node.getNodeData());
            numberElement ++;
            if (node != getLastNode()) stack.append(", ");
        }
        stack.append("}");
        return stack.toString();
    }

    /**
     * Este metodo es como el anterior pero para tener los elementos de la pila en un String para mostrarlo en panta;
     * @return
     */
    public String getOperationString(){
        StringBuilder stack = new StringBuilder();
        for (Nodo<T> node = mainNode; node != null; node = node.getNextNode()){
            stack.append(node.getNodeData());
        }
        return stack.toString();
    }

    public int size() {
        int size = 0;
        for (Nodo<T> node = mainNode; node != null; node = node.getNextNode()) size++;
        return size;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public Nodo<T> getMainNode() {
        return mainNode;
    }
}
