package Stack_Task16;

public class CalculatorView {

    final String BORDERS;
    final String EXCEPTIONBORDERS;
    CalculatorValidator validator;
    final int LIMITNUMBER;

    public CalculatorView() {
        BORDERS = "-";
        EXCEPTIONBORDERS = "*";
        LIMITNUMBER = -2147483648;
        validator = new CalculatorValidator();
    }

    public void showResult(String operation, int result) {
        if (result <= LIMITNUMBER || result == 0) {
            exceptionOutOfRange(operation);
        } else {
            System.out.println(BORDERS.repeat(64)+"\n");
            System.out.println("\tOPERATION\t:\t" + operation + "\t|\tRESULT\t:\t" + result);
            System.out.println("\n"+BORDERS.repeat(64));
        }
    }

    public void invalidOperationException(String operation) {
        validator.isValid(operation);
        System.out.println(EXCEPTIONBORDERS.repeat(64));
        System.out.println("\n\tERROR !\n\tOPERATION\t:\t" + operation);
        if (!validator.validateParentheses()) parenthesesException();
        if (!validator.validateOperators()) operatorsException();
        System.out.println("\n"+ EXCEPTIONBORDERS.repeat(64));
    }

    private void operatorsException() {
        System.out.println("\tError in the operators");
    }

    private void parenthesesException() {
        System.out.println("\tError in the parentheses");
    }

    private void exceptionOutOfRange(String operation) {
        System.out.println(EXCEPTIONBORDERS.repeat(64)+"\n");
        System.out.println("\tOPERATION\t:\t" + operation);
        System.out.println("\tResult out of range");
        System.out.println("\n"+EXCEPTIONBORDERS.repeat(64));
    }
}
