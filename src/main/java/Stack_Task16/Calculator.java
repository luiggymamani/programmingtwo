package Stack_Task16;

public class Calculator {

    Stack<Character> operation;
    Operations operations;
    CalculatorValidator validator;
    CalculatorView calculatorView;

    public Calculator() {
        operation = new Stack<>();
        operations = new Operations();
        validator = new CalculatorValidator();
        calculatorView = new CalculatorView();
    }

    private Stack<Character> generateTokens(String operation) {
        Stack<Character> tokens = new Stack<>();
        for (int index = 0; index < operation.length(); index++) {
            tokens.push(operation.charAt(index));
        }
        return tokens;
    }

    public int solveOperation(String operationString) {
        /*
        voy a usar la variable 'operationString' para mostrar la operacion al final
        y para crear otra pila con la misma operacion para comprobar si es valida.
         */

        int result = 0;
        operation = generateTokens(operationString);

        if (validator.isValid(operationString)){
            operation = reverseStack(operation);
            result = solveOperation(operation);
            calculatorView.showResult(operationString, result);
        } else {
            calculatorView.invalidOperationException(operationString);
        }
        return result;
    }

    private int solveOperation(Stack<Character> operation) {
        int number, result = 0;
        char operator = ' ', character;
        while (!operation.isEmpty()) {
            character = operation.pop();
            if (result == 0) {
                if (character == '(') result = operationWithParentheses(operation);
                else result = findNumber(character, operation);
            } else {
                if (operator == ' '){
                    operator = character;
                    if (operator == '!'){
                        result = solveOperation(operator, result, 0);
                        operator = ' ';
                    }
                } else {
                    if (character == '(') number = operationWithParentheses(operation);
                    else number = findNumber(character, operation);
                    result = solveOperation(operator, result, number);
                    operator = ' ';
                    number = 0;
                }
            }
        }
        return result;
    }

    private int operationWithParentheses(Stack<Character> tokens) {
        int number, result = 0;
        char character;
        Stack<Character> parenthesisOperation = new Stack<>();

        while (!tokens.isEmpty()) {
            character = tokens.pop();
            if (character == '(') {
                number = operationWithParentheses(tokens);
                parenthesisOperation = getNumbers(parenthesisOperation, number);
                character = tokens.pop();
            } if (character == ')') {
                parenthesisOperation = reverseStack(parenthesisOperation);
                result =  solveOperation(parenthesisOperation);
                break;
            } else parenthesisOperation.push(character);
        }
        return result;
    }

    public int findNumber(char character, Stack<Character> operation) {
        int number = Integer.parseInt(String.valueOf(character));

        while (!operation.isEmpty()) {
            character = operation.pop();
            if (Character.isDigit(character)){
                number = Integer.parseInt(number + "" + character);
                continue;
            } else operation.push(character); break;
        }

        return number;
    }

    private int solveOperation(char operator, int firstNumber, int secondNumber){
        switch (operator) {
            case '+':
                return operations.addition(firstNumber, secondNumber);
            case '*':
                return operations.multiplication(firstNumber, secondNumber);
            case '^':
                return operations.power(firstNumber, secondNumber);
            case '!':
                return operations.factorial(firstNumber);
            default:
                return firstNumber;
        }
    }

    private Stack<Character> reverseStack(Stack<Character> stack) {
        Stack<Character> invertedStack = new Stack<>();
        while (!stack.isEmpty())
            invertedStack.push(stack.pop());
        return invertedStack;
    }

    /**
     * Este metodo agregara los digitos del numero a la pila que se le paso como parametro
     *
     * Al principio use el codigo que esta comentado, lo que hace es:
     * 1. agregar cada digito a una nueva pila de tipo integer
     * 2. luego invertir la pila (para sacarlos en orden)
     * 3. con la pila invertida saco los valores, los casteo a char y los agrego a la pila principal pero
     * Al momento de agregarlo no se agregaban los numeros en char sino caracteres vacios.
     *
     * Por eso use String para:
     * 1. Tener el numero en string
     * 2. con un foreach saco cada caracter del String
     * 3. Ese caracter lo agrego a la pila
     *
     * En pocas palabras use String porque no funcionada el casting de int a char.
     *
     * @param parenthesisOperation -
     * @param number -
     * @return -
     */
    private Stack<Character> getNumbers(Stack<Character> parenthesisOperation, int number) {
        String numberStr = Integer.toString(number);
        for (char character : numberStr.toCharArray())
            parenthesisOperation.push(character);

        /*int unitNumber;
        Stack<Integer> integerStack = new Stack<>();
        while (number > 0) {
            unitNumber = number % 10;
            integerStack.push(unitNumber);
            number = number / 10;
        }

        Stack<Integer> invertedStack = new Stack<>();
        while (!integerStack.isEmpty())
            invertedStack.push(integerStack.pop());

        while (!invertedStack.isEmpty()){
            unitNumber = invertedStack.pop();
            parenthesisOperation.push((char)unitNumber);
        }*/

        return parenthesisOperation;
    }

}
