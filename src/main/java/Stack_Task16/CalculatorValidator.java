package Stack_Task16;

public class CalculatorValidator {

    Stack<Character> tokens;
    char character;
    int openParentheses;
    int closedParentheses;
    boolean validOperators;

    public CalculatorValidator() {
        character = ' ';
        openParentheses = 0;
        closedParentheses = 0;
        validOperators = true;
        tokens = new Stack<>();
    }

    public boolean isValid(String operation) {
        processOperation(operation);
        return validateParentheses() && validateOperators();
    }

    public void poorlyPositionedOperators() {
        if (!Character.isDigit(tokens.getMainNode().getNodeData()) &&
                tokens.getMainNode().getNodeData() != '(')
            validOperators = false;
        if (!Character.isDigit(tokens.getLastNode().getNodeData()) &&
                tokens.getLastNode().getNodeData() != '!' && tokens.getLastNode().getNodeData() != ')')
            validOperators = false;
    }

    public boolean validateParentheses() {
        return openParentheses == closedParentheses;
    }

    public boolean validateOperators() {
        return validOperators;
    }

    private void processOperation(String operation) {
        for (int index = 0; index < operation.length(); index++) tokens.push(operation.charAt(index));
        poorlyPositionedOperators();
        tokens = reverseStack(tokens);
        while (!tokens.isEmpty()) {
            character = tokens.pop();
            if (!Character.isDigit(character)) {
                if (character == '(') openParentheses ++;
                if (character == ')') closedParentheses ++;
                if (character == '/'|| character == '-') validOperators = false;
                if (!tokens.isEmpty()){
                    character = tokens.pop();
                    if (!Character.isDigit(character) && character != ')' && character != '(' && character != '+' &&
                            character != '*' && character != '!' && character != '^')
                        validOperators = false;
                    else tokens.push(character);
                }
            }
        }
    }

    private Stack<Character> reverseStack(Stack<Character> stack) {
        Stack<Character> invertedStack = new Stack<>();
        while (!stack.isEmpty())
            invertedStack.push(stack.pop());
        return invertedStack;
    }
}
