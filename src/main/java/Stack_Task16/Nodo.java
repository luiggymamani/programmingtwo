package Stack_Task16;

public class Nodo<T> {

    T nodeData;
    Nodo<T> nextNode;

    public Nodo(T nodeData) {
        this.nodeData = nodeData;
        nextNode = null;
    }

    public T getNodeData() {
        return nodeData;
    }

    public Nodo<T> getNextNode() {
        return nextNode;
    }

    public void setNextNode(Nodo<T> nextNode) {
        this.nextNode = nextNode;
    }
}
