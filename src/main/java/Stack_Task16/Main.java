package Stack_Task16;

public class Main {

    public static void main(String[] args) {

        Calculator calculator = new Calculator();

        //Operaciones basadas en el ejemplo de la tarea:
        calculator.solveOperation("1+2+3"); // 6
        calculator.solveOperation("2*2*5"); // 20
        calculator.solveOperation("2^3+2"); // 10
        calculator.solveOperation("1+2*5"); // 15
        calculator.solveOperation("4^2*3"); // 48

        calculator.solveOperation("(1+2)+3"); // 6
        calculator.solveOperation("2*(2*5)"); // 20
        calculator.solveOperation("(2^3)+2"); // 10
        calculator.solveOperation("2^(3+2)"); // 32
        calculator.solveOperation("(1+2)*5"); //15
        calculator.solveOperation("1+(2*5)"); // 11
        calculator.solveOperation("4^(2*3)"); // 4096
        calculator.solveOperation("3!^2"); // 36
        calculator.solveOperation("5!+10");// 130
        calculator.solveOperation("(3+2)!"); //120
        calculator.solveOperation("3^(3!)+(5*2)"); //739
        calculator.solveOperation("(3!)^2"); // 36
        calculator.solveOperation("((3!)^2)+(2+3)"); // 41
        calculator.solveOperation("((3^(3!))+5)*2"); // 1468

        calculator.solveOperation("1000!"); // out of range
        calculator.solveOperation("2147483647+1"); // out of range
        calculator.solveOperation("6/3-1"); //exception
        calculator.solveOperation("(((3^(3!))+5)*-2))"); //exception
    }
}


