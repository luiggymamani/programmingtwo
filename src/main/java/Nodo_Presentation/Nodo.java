package Nodo_Presentation;

public class Nodo {

    public int nodeData;
    public Nodo nextNode;

    public Nodo(int nodeData) {
        this.nodeData = nodeData;
        nextNode = null;
    }

    public int getNodeData() {
        return nodeData;
    }

    public void setNextNode(Nodo nextNode) {
        this.nextNode = nextNode;
    }

    public Nodo getNextNode() {
        return nextNode;
    }
}
