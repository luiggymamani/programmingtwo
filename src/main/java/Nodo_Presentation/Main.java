package Nodo_Presentation;

public class Main {

    public static void main(String[] args) {

        Nodo firstNode = new Nodo(10);
        Nodo secondNode = new Nodo(20);
        Nodo thirdNode = new Nodo(30);

        System.out.println("\n-------------------Node------------------");
        firstNode.setNextNode(secondNode);
        firstNode.getNextNode().setNextNode(thirdNode);

        System.out.println("First Node: " + firstNode.getNodeData());
        System.out.println("Second Node: " + firstNode.getNextNode().getNodeData());
        System.out.println("Third Node: " + firstNode.getNextNode().getNextNode().getNodeData());

        System.out.println("\n---------------Node List-----------------");
        NodeList nodeList = new NodeList();
        nodeList.add(10);
        nodeList.add(20);
        nodeList.add(30);

        System.out.println("Size: " + nodeList.size());
        System.out.println("First Node: " + nodeList.get(0));
        System.out.println("Second Node: " + nodeList.get(1));
        System.out.println("Third Node: " + nodeList.get(2));
    }
}
