package Nodo_Presentation;

public class NodeList {

    Nodo mainNode;
    int size;

    public NodeList(){
        mainNode = null;
        size = 0;
    }

    public boolean isEmpty(){
        return mainNode == null;
    }

    public void add(int nodeData){
        if (isEmpty()){
            mainNode = new Nodo(nodeData);
        } else {
            Nodo oldMainNode = mainNode;
            Nodo newNode = new Nodo(nodeData);
            newNode.setNextNode(oldMainNode);
            mainNode = newNode;
        } size ++;
    }

    public int size(){
        return size;
    }

    public int get(int index){
        Nodo nodeToCompare = mainNode;
        for (int count = 0; count < index; count++) {
            nodeToCompare = nodeToCompare.getNextNode();
        };
        return nodeToCompare.getNodeData();
    }
}
