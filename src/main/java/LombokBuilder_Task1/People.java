package LombokBuilder_Task1;

import java.util.ArrayList;

/**
 * This class represents a set of people
 *
 * @author Luiggy
 */
public class People{

    private int peopleLimit;
    private ArrayList<Person> peopleList;

    /**
     * This is a constructor method.
     *
     * @param peopleLimit is the number of people who can be on the people list
     */
    public People(int peopleLimit) {
        this.peopleLimit = peopleLimit;
        this.peopleList = new ArrayList<>();
    }

    /**
     * This method add a person to people List.
     *
     * The person will only be added if the limit is not exceeded.
     * @param person is the person to add.
     */
    public void addPersonToPeopleList(Person person){
        if(peopleList.size() < peopleLimit){
            peopleList.add(person);
        }
    }

    public int getPeopleLimit() {
        return peopleLimit;
    }

    public void setPeopleLimit(int peopleLimit) {
        this.peopleLimit = peopleLimit;
    }

    public ArrayList<Person> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(ArrayList<Person> peopleList) {
        this.peopleList = peopleList;
    }
}
