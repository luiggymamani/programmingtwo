package LombokBuilder_Task1;

public class Main {

    public static void main(String [] args){

        Person personOne = new Person("Hugo", "Ramirez", 20);
        Person personTwo = new Person("Pedro", "Gadez", 22);
        Person personThree = new Person("Jessica", "Moreno", 21);

        System.out.println(personOne.greet(personOne.getGREET(), personOne.getName()));
        System.out.println(personTwo.greet("Hi", personTwo.getName()));
        System.out.println(personThree.greet("Good morning", personThree.getName()));

        People people = new People(2);
        people.addPersonToPeopleList(personTwo);
        people.addPersonToPeopleList(personThree);
        people.addPersonToPeopleList(personOne);

    }
}
