package LombokBuilder_Task1;

/**
 * This class represents a person.
 *
 * @author Luiggy
 */
public class Person {

    private String name;
    private String lastName;
    private int age;
    private final String GREET;

    /**
     * This is a constructor method, for the characteristics of a person.
     *
     * @param name is the name of the person.
     * @param lastName is the lastname of the person.
     * @param age is the age of the person.
     */
    public Person(String name, String lastName, int age) {
        this.age = age;
        this.name = name;
        this.lastName = lastName;
        this.GREET = "hello";
    }

    /**
     * This method simulates the action of greeting.
     *
     * @param GREET it's the greeting of the person
     * @param namePerson it's the name of the person
     * @return the greeting.
     */
    public String greet(String GREET, String namePerson){
        return GREET + " my name is " + namePerson;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGREET() {
        return GREET;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
