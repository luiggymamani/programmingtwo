package Clase30_08;

public class Main {

    public static void main(String[] args) {

        Queue<Integer> queue = new Queue<>();
        queue.add(1);
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(3);
        queue.add(4);
        queue.add(4);
        queue.add(5);
        queue.add(3);
        queue.add(5);

        System.out.println(queue.showQueue());
        System.out.println(queue.getHead().getNodeData());
        System.out.println(queue.getTail().getNodeData());

    }
}
