package Clase30_08;

public class Queue<T> {

    Nodo<T> head;
    Nodo<T> tail;
    int size;

    public Queue() {
        head = null;
        tail = null;
        size = 0;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public void add(T element) {
        if (!contains(element)) {
            Nodo<T> newNode = new Nodo<>(element);
            if (isEmpty()) head = newNode;
            else newNode.setNextNode(tail);
            tail = newNode;
            size++;
        }
    }

    private boolean contains(T element) {
        for (Nodo<T> node = tail; node != null; node = node.getNextNode()) {
            if (node.getNodeData().equals(element)) return true;
        }
        return false;
    }

    public String showQueue(){
        StringBuilder stack = new StringBuilder();
        for (Nodo<T> node = tail; node != null; node = node.getNextNode()){
            stack.append(node.getNodeData());
            if (node != head) stack.append(", ");
        }
        return stack.toString();
    }

    public T deque() {
        T data = null;
        if (!isEmpty()){
            data = head.getNodeData();
            for (Nodo<T> node = tail; node != null; node = node.getNextNode()){
                if (node.getNextNode() == head) {
                    node.setNextNode(null);
                    head = node;
                } else if (head == tail) {
                    tail = null;
                    head = null;
                }
            }
            size--;
        }
        return data;
    }

    public static Queue<Integer> insert(Queue<Integer> queue1, Queue<Integer> queue2) {
        Queue<Integer> newQueue = new Queue<>();
        while (!queue1.isEmpty() && !queue2.isEmpty()) {
            newQueue.add(queue1.deque());
            newQueue.add(queue2.deque());
        }
        return newQueue;
    }

    public Nodo<T> getHead() {
        return head;
    }

    public Nodo<T> getTail() {
        return tail;
    }
}
