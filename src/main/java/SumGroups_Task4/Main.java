package SumGroups_Task4;

public class Main {

    public static void main(String[] args){

        int[] array = {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9};
        ArrayLogic arrayLogic = new ArrayLogic(array);
        arrayLogic.showAllResult();
        //  [2, 1, 10, 5, 2, 24, 4, 15]

        int[] arrayTwo = {4, 6, 10, 3, 6, 1, 5, 3, 0, 2, 2, 1, 2, 7, 8, 10, 2};
        ArrayLogic arrayLogicTwo = new ArrayLogic(arrayTwo);
        arrayLogicTwo.showAllResult();
        //  [20, 3, 6, 9, 4, 1, 2, 7, 20]

        int[] arrayThree = {9, 9, 1, 2, 6, 2, 1, 2, 1, 7, 7, 3, 3, 4, 7, 7, 9};
        ArrayLogic arrayLogicThree = new ArrayLogic(arrayThree);
        arrayLogicThree.showAllResult();
        //  [19, 10, 1, 2, 21, 4, 23]
    }
}
