package SumGroups_Task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayLogic {

    private int mainIndex;
    private int endOfIndex;
    private int[] array;
    List<Integer> entireToursList;

    public ArrayLogic(int[] array) {
        this.array = array;
        entireToursList = new ArrayList<>();
        mainIndex = 0;
        endOfIndex = 0;
    }

    public void analyzeTheList(){
        int mainNumber;
        int nextNumber;
        int sumOfNumbers;
        boolean isEvenMainNumber;
        boolean isEvenNextNumber;

        for (mainIndex = 0; mainIndex < array.length; mainIndex++) {
            mainNumber = array[mainIndex];

            if(mainIndex < array.length - 1){
                nextNumber = array[mainIndex + 1];
            } else {
                break;
            }

            sumOfNumbers = 0;
            isEvenMainNumber = checkEvenNumber(mainNumber);
            isEvenNextNumber = checkEvenNumber(nextNumber);

            if(isEvenMainNumber && isEvenNextNumber){
                endOfIndex = findGroupNumbers(mainIndex, true);
                sumGroupNumbers(sumOfNumbers);
            } else if (!isEvenMainNumber && !isEvenNextNumber) {
                endOfIndex = findGroupNumbers(mainIndex, false);
                sumGroupNumbers(sumOfNumbers);
            } else {
                entireToursList.add(mainNumber);
            }
        }
    }

    private void sumGroupNumbers(int sumOfNumbers){
        if(endOfIndex == array.length - 1){
            sumOfNumbers = sumFinalNumbers(sumOfNumbers);
        } else {
            sumOfNumbers = sumNumbers(sumOfNumbers);
        }
        entireToursList.add(sumOfNumbers);
    }

    public boolean checkEvenNumber(int numberToVerify){
        boolean isEvenNumber = false;
        if (numberToVerify % 2 == 0){
            isEvenNumber = true;
        }
        return isEvenNumber;
    }

    private int sumNumbersFromArray(int sumOfNumbers, int index){
        sumOfNumbers += array[index];
        mainIndex = endOfIndex - 1;
        return sumOfNumbers;
    }

    private int sumFinalNumbers(int sumOfNumbers){
        for (int index = mainIndex; index <= endOfIndex; index++) {
            sumOfNumbers = sumNumbersFromArray(sumOfNumbers, index);
        }
        return sumOfNumbers;
    }

    private int sumNumbers(int sumOfNumbers){
        for (int index = mainIndex; index < endOfIndex; index++) {
            sumOfNumbers = sumNumbersFromArray(sumOfNumbers, index);
        }
        return sumOfNumbers;
    }
    private int findGroupNumbers(int startingIndex, boolean isEvenMainNumber){
        int numberToVerify;
        int endIndex = startingIndex;

        for (int index = startingIndex; index < array.length; index++) {
            numberToVerify = array[index];

            if (index == array.length - 1){
                endIndex = index;
                break;
            } else if (isEvenMainNumber) {
                if (!checkEvenNumber(numberToVerify)){
                    endIndex = index;
                    break;
                }
            } else {
                if (checkEvenNumber(numberToVerify)){
                    endIndex = index;
                    break;
                }
            }

        }
        return endIndex;
    }

    public void updateTheNewArray(){
        int[] newArray = new int[entireToursList.size()];
        for (int index = 0; index < entireToursList.size(); index++) {
            newArray[index] = entireToursList.get(index);
        }
        array = newArray;
    }

    public void showArray(){
        System.out.println("\n" + Arrays.toString(array));
    }

    public void showAllResult(){
        analyzeTheList();
        updateTheNewArray();
        showArray();
    }

    public int[] getArray() {
        return array;
    }
}
