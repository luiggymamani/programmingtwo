package FindTheGreatestNumberOfRepetitions_Task3;

public class Main {

    public static void main(String[] args){
        int[] inputArray = new int[13];
        inputArray[0] = 3;
        inputArray[1] = -1;
        inputArray[2] = -1;
        inputArray[3] = -1;
        inputArray[4] = 2;
        inputArray[5] = 3;
        inputArray[6] = -1;
        inputArray[7] = 3;
        inputArray[8] = -1;
        inputArray[9] = 2;
        inputArray[10] = 4;
        inputArray[11] = 9;
        inputArray[12] = 3;


        //[3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3]
        LogicFind logicFind = new LogicFind(inputArray);
        //  first case
        logicFind.showInfoToFindNumber();

        //  second case
        logicFind.generateArray(13);
        logicFind.showInfoToFindNumber();
    }
}
