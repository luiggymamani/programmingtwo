package FindTheGreatestNumberOfRepetitions_Task3;

import java.util.Arrays;

public class LogicFind {

    private int[] array;
    private final int limitNumberForRandom;

    public LogicFind(int[] array) {
        limitNumberForRandom = 5;
        this.array = array;
    }

    private int countReps(int number){
        int count = 0;
        for (int numberToCompare : array) {
            if (number == numberToCompare){
                count ++;
            }
        }
        return count;
    }

    public int findNumberByRepetitions(){
        int numberOfHigherRepetitions = 0;
        int repsNumber;
        int repsNumberToCompare;

        for (int number : array) {
            numberOfHigherRepetitions = number;
            repsNumber = countReps(number);
            for (int numberToCompare : array) {
                repsNumberToCompare = countReps(numberToCompare);
                if(repsNumberToCompare > repsNumber){
                    numberOfHigherRepetitions = numberToCompare;
                    break;
                }
            }
        }
        return numberOfHigherRepetitions;
    }

    private void printArray(){
        System.out.println("\n" + Arrays.toString(array));
    }

    public void generateArray(int amountArray){
        int[] generatedArray = new int[amountArray];
        int numberRandom;

        for (int i = 0; i < generatedArray.length; i++) {
            numberRandom = (int)(Math.random() * limitNumberForRandom + 1);
            generatedArray[i] = numberRandom;
        }
        array = generatedArray;
    }

    public void showInfoToFindNumber(){
        int numberOfHigherRepetitions = findNumberByRepetitions();
        printArray();
        System.out.println("Te number Of Higher Repetitions : " + numberOfHigherRepetitions);
        System.out.println("Reps: " + countReps(numberOfHigherRepetitions));
    }

    /*public int findNumberByRepetitions(){
        int numberOfHigherRepetitions = 0;
        int number;
        int numberToCompare;
        int repsNumber;
        int repsNumberToCompare;

        for (int index = 0; index < array.length; index++) {
            number = array[index];
            numberOfHigherRepetitions = number;
            repsNumber = countReps(number);
            for (int i = 0; i < array.length; i++) {
                numberToCompare = array[i];
                repsNumberToCompare = countReps(numberToCompare);
                if(repsNumberToCompare > repsNumber){
                    numberOfHigherRepetitions = numberToCompare;
                    i = array.length;
                }
            }
        }
        return numberOfHigherRepetitions;
    }*/
}
