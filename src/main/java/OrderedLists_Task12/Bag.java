package OrderedLists_Task12;

public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();

    /**
     * This is an extra sort method
     *
     * This method will compare the value of an element
     * of the linked list with the value of the previous node and if this
     * is greater the nodes will change their position.
     */
    void insertionSort();

    /*
    change the x element position to y position and
    y element position to x position
    0 1 2 3 4
    1,2,3,4,5
    xchange(1,3) =>
    1,4,3,2,5
    * */
    void xchange(int y, int x);

}




