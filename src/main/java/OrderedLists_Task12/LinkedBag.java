package OrderedLists_Task12;

public class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;

    @Override
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    @Override
    public void selectionSort() {
        Integer firstInt;
        Integer secondInt;

        for (int mainIndex = 0; mainIndex < size - 1; mainIndex++) {
            for (int indexToCompare = mainIndex + 1; indexToCompare < size; indexToCompare++) {
                firstInt = (Integer) getNodeData(mainIndex);
                secondInt = (Integer) getNodeData(indexToCompare);
                if (firstInt > secondInt) {
                    xchange(mainIndex, indexToCompare);
                }
            }
        }
    }

    @Override
    public void bubbleSort() {
        Integer firstInt;
        Integer secondInt;
        boolean sorted;

        do {
            sorted = true;
            for (int mainIndex = 0; mainIndex < size - 1; mainIndex++) {
                firstInt = (Integer) getNodeData(mainIndex);
                secondInt = (Integer) getNodeData(mainIndex + 1);
                if (firstInt > secondInt){
                    xchange(mainIndex, mainIndex + 1);
                    sorted = false;
                }
            }

        } while (!sorted);
    }

    @Override
    public void insertionSort() {
        for (int index = 0; index < size; index++) {
            int position = index;
            while (position > 0 && (Integer)getNodeData(position - 1) > (Integer)getNodeData(position)){
                xchange(position, position - 1);
                position --;
            }
            setNode(position, getNodeData(position));
        }
    }

    @Override
    public void xchange(int firstIndex, int secondIndex) {
        T auxData = getNode(firstIndex).getData();
        setNode(firstIndex, getNode(secondIndex).getData());
        setNode(secondIndex, auxData);
    }

    private void setNode(int index, T element) {
        Node<T> newNode = new Node<>(element);
        Node<T> currentNode = getNode(index);
        Node<T> nextNode = currentNode.getNext();

        if (index - 1 < 0){
            newNode.setNext(nextNode);
            root = newNode;
        } else {
            Node<T> previousNode = getNode(index - 1);
            previousNode.setNext(newNode);
            newNode.setNext(nextNode);
        }
    }

    private Node<T> getNode(int index) {
        Node<T> node = root;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }
        return node;
    }

    private T getNodeData(int index) {
        return getNode(index).getData();
    }

}
