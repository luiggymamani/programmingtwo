import ListAppend_Task10.Nodo;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Task10Test {

    private Nodo generateListA() {
        Nodo listA = new Nodo(1);
        listA.setNextNode(new Nodo(2));
        listA.getNextNode().setNextNode(new Nodo(3));
        return listA;
    }

    private Nodo generateListB() {
        Nodo listB = new Nodo(4);
        listB.setNextNode(new Nodo(5));
        listB.getNextNode().setNextNode(new Nodo(6));
        return listB;
    }

    @Test
    public void twoEmpty() throws Exception {
        assertNull( Nodo.append( null, null ) );
    }

    @Test
    public void oneEmpty() throws Exception {
        Nodo listAppend = Nodo.append(generateListA(), null);
        String waitingListView = "1 -> 2 -> 3 -> ";
        assertEquals(waitingListView, Nodo.generateViwList(listAppend));

        listAppend = Nodo.append(null, generateListB());
        waitingListView = "4 -> 5 -> 6 -> ";
        assertEquals(waitingListView, Nodo.generateViwList(listAppend));
    }

    @Test
    public void oneOne() throws Exception {
        Nodo listAppend = Nodo.append(new Nodo(1), new Nodo(2));
        String waitingListView = "1 -> 2 -> ";
        assertEquals(waitingListView, Nodo.generateViwList(listAppend));

        listAppend = Nodo.append(new Nodo(2), new Nodo(1));
        waitingListView = "2 -> 1 -> ";
        assertEquals(waitingListView, Nodo.generateViwList(listAppend));
    }

    @Test
    public void bigLists() throws Exception {
        Nodo listAppend = Nodo.append(generateListA(), generateListB());
        String waitingListView = "1 -> 2 -> 3 -> 4 -> 5 -> 6 -> ";
        assertEquals(waitingListView, Nodo.generateViwList(listAppend));
    }
}
