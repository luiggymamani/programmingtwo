import ImplementInterface_Task7.IntegersIterator;
import ImplementInterface_Task7.JUArrayList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Task7Test {

    @Test
    public void testSize(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(10);
        juArrayList.add(10);

        assertEquals(3, juArrayList.size());
    }

    @Test
    public void testIsEmpty(){
        JUArrayList juArrayList = new JUArrayList();
        assertTrue(juArrayList.isEmpty());
    }

    @Test
    public void testAdd(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);

        int[] arrayListToCompare = {10, 20, 30};
        assertArrayEquals(arrayListToCompare, juArrayList.getArrayList());
    }

    @Test
    public void testAddWithIndex(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);
        juArrayList.add(1, 1000);

        int[] arrayListToCompare = {10, 1000, 20, 30};
        assertArrayEquals(arrayListToCompare, juArrayList.getArrayList());
    }

    @Test
    public void testRemoveWithIndex(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);
        juArrayList.remove(1);

        int[] arrayListToCompare = {10, 30};
        assertArrayEquals(arrayListToCompare, juArrayList.getArrayList());
    }

    @Test
    public void testRemoveWithObject(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);
        juArrayList.remove((Object)30);

        int[] arrayListToCompare = {10, 20};
        assertArrayEquals(arrayListToCompare, juArrayList.getArrayList());
    }

    @Test
    public void testClear(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);
        juArrayList.clear();

        int[] arrayListToCompare = new int[0];
        assertArrayEquals(arrayListToCompare, juArrayList.getArrayList());
    }

    @Test
    public void testGet(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);

        assertEquals(30, juArrayList.get(2));
    }

    @Test
    public void testContains(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(20);

        assertTrue(juArrayList.contains(20));
    }

    @Test
    public void testSet(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(10);
        juArrayList.add(20);
        juArrayList.add(30);
        juArrayList.set(1, 1000);

        int[] arrayListToCompare = {10, 1000, 30};
        assertArrayEquals(arrayListToCompare, juArrayList.getArrayList());
    }
}
