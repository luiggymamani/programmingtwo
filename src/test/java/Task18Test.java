import RoundRobin.Task;
import RoundRobin.TaskCreator;
import RoundRobin.TaskExecutor;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task18Test {

    @Test
    public void testTaskCreator() {
        TaskCreator taskCreator = new TaskCreator();

        ArrayList<Task> tasks = taskCreator.generateTasks(3);
        int expectedTasks = 3;
        assertEquals(expectedTasks, tasks.size());

        tasks = taskCreator.generateTasks(6);
        expectedTasks = 6;
        assertEquals(expectedTasks, tasks.size());

        tasks = taskCreator.generateTasks(10);
        expectedTasks = 10;
        assertEquals(expectedTasks, tasks.size());
    }

    @Test
    public void testTaskExecutorOneTask() {
        TaskExecutor executor1 = new TaskExecutor(10);
        Task task = new Task("intelliJ", 13);
        executor1.addTask(task);

        executor1.executeTasks();
        int expectedCPUBursts = 3;
        assertEquals(expectedCPUBursts, task.getCpuBurst());

        executor1.executeTasks();
        expectedCPUBursts = 0;
        assertEquals(expectedCPUBursts, task.getCpuBurst());


        TaskExecutor executor2 = new TaskExecutor(5);
        task = new Task("VLC", 5);
        executor2.addTask(task);

        executor2.executeTasks();
        assertEquals(expectedCPUBursts, task.getCpuBurst());


        TaskExecutor executor3 = new TaskExecutor(3);
        task = new Task("Terminal", 2);
        executor3.addTask(task);

        executor3.executeTasks();
        assertEquals(expectedCPUBursts, task.getCpuBurst());
    }

    @Test
    public void testQueueOrder() {
        TaskExecutor executor = new TaskExecutor(5);
        ArrayList<Task> tasks = new ArrayList<>();
        tasks.add(new Task("intelliJ", 13));
        tasks.add(new Task("Teams", 4));
        tasks.add(new Task("VSCode", 8));
        tasks.add(new Task("Terminal", 2));
        tasks.add(new Task("Firefox", 7));
        executor.addTasks(tasks);
        Queue<Task> taskQueue = executor.getTaskQueue();

        String expectedNameTask = "intelliJ";
        assertEquals(expectedNameTask, Objects.requireNonNull(taskQueue.poll()).getNameTask());

        expectedNameTask = "Teams";
        assertEquals(expectedNameTask, Objects.requireNonNull(taskQueue.poll()).getNameTask());

        expectedNameTask = "VSCode";
        assertEquals(expectedNameTask, Objects.requireNonNull(taskQueue.poll()).getNameTask());

        expectedNameTask = "Terminal";
        assertEquals(expectedNameTask, Objects.requireNonNull(taskQueue.poll()).getNameTask());

        expectedNameTask = "Firefox";
        assertEquals(expectedNameTask, Objects.requireNonNull(taskQueue.poll()).getNameTask());
    }

    @Test
    public void testTaskExecutorManyTask() {
        TaskExecutor executor = new TaskExecutor(5);
        ArrayList<Task> tasks = new ArrayList<>();
        tasks.add(new Task("intelliJ", 13));
        tasks.add(new Task("Teams", 4));
        tasks.add(new Task("VSCode", 8));
        tasks.add(new Task("Terminal", 2));
        tasks.add(new Task("Firefox", 7));
        executor.addTasks(tasks);


        // First execution of tasks   -->   size : 5
        int expectedSize = 5;
        assertEquals(expectedSize, executor.getTaskQueue().size());

        executor.executeTasks();
        Task runningTask = executor.getRunningTask();
        String nameTask = "intelliJ";
        assertEquals(nameTask, runningTask.getNameTask());
        int expectedTimeStarted = 0;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        int expectedFinishedTime = 5;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        int expectedCPUBursts = 8;
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());

        executor.executeTasks();
        runningTask = executor.getRunningTask();
        nameTask = "Teams";
        assertEquals(nameTask, runningTask.getNameTask());
        expectedTimeStarted = 6;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        expectedFinishedTime = 10;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        expectedCPUBursts = 0;
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());

        executor.executeTasks();
        runningTask = executor.getRunningTask();
        nameTask = "VSCode";
        assertEquals(nameTask, runningTask.getNameTask());
        expectedTimeStarted = 11;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        expectedFinishedTime = 16;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        expectedCPUBursts = 3;
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());

        executor.executeTasks();
        runningTask = executor.getRunningTask();
        nameTask = "Terminal";
        assertEquals(nameTask, runningTask.getNameTask());
        expectedTimeStarted = 17;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        expectedFinishedTime = 19;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        expectedCPUBursts = 0;
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());

        executor.executeTasks();
        runningTask = executor.getRunningTask();
        nameTask = "Firefox";
        assertEquals(nameTask, runningTask.getNameTask());
        expectedTimeStarted = 20;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        expectedFinishedTime = 25;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        expectedCPUBursts = 2;
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());


        // Second execution of tasks   -->   size : 3
        expectedSize = 3;
        assertEquals(expectedSize, executor.getTaskQueue().size());

        executor.executeTasks();
        runningTask = executor.getRunningTask();
        nameTask = "intelliJ"; // 8
        assertEquals(nameTask, runningTask.getNameTask());
        expectedTimeStarted = 26;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        expectedFinishedTime = 31;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        expectedCPUBursts = 3;
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());

        executor.executeTasks();
        runningTask = executor.getRunningTask();
        nameTask = "VSCode"; // 3
        assertEquals(nameTask, runningTask.getNameTask());
        expectedTimeStarted = 32;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        expectedFinishedTime = 35;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        expectedCPUBursts = 0;
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());

        executor.executeTasks();
        runningTask = executor.getRunningTask();
        nameTask = "Firefox";  // 2
        assertEquals(nameTask, runningTask.getNameTask());
        expectedTimeStarted = 36;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        expectedFinishedTime = 38;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());


        // Third execution of tasks   -->   size : 1
        expectedSize = 1;
        assertEquals(expectedSize, executor.getTaskQueue().size());

        executor.executeTasks();
        runningTask = executor.getRunningTask();
        nameTask = "intelliJ"; // 3
        assertEquals(nameTask, runningTask.getNameTask());
        expectedTimeStarted = 39;
        assertEquals(expectedTimeStarted, runningTask.getTimeStarted());
        expectedFinishedTime = 42;
        assertEquals(expectedFinishedTime, runningTask.getFinishedTime());
        assertEquals(expectedCPUBursts, runningTask.getCpuBurst());


        // All tasks were executed
        expectedSize = 0;
        assertEquals(expectedSize, executor.getTaskQueue().size());
        int expectedTotalTime = 42;
        assertEquals(expectedTotalTime, executor.getTime());

    }
}
