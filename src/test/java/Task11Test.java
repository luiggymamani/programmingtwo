import ListGenericos_Task11.JULinkedList;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class Task11Test {

    private JULinkedList<String> generateListString(){
        JULinkedList<String> stringList = new JULinkedList<>();
        stringList.add("String 1");
        stringList.add("String 2");
        stringList.add("String 3");
        stringList.add("String 4");
        stringList.add("String 5");
        stringList.add("String 3");
        return stringList;
    }

    private JULinkedList<Integer> generateListInteger(){
        JULinkedList<Integer> integerList = new JULinkedList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(5);
        integerList.add(3);
        return integerList;
    }

    @Test
    public void testSize(){
        assertEquals(6, generateListString().size());
        assertEquals(6, generateListInteger().size());
    }

    @Test
    public void testIsEmpty(){
        List<String> stringList = new JULinkedList<>();
        assertTrue(stringList.isEmpty());
        stringList = generateListString();
        assertFalse(stringList.isEmpty());

        List<Integer> integerList = new JULinkedList<>();
        assertTrue(integerList.isEmpty());
        integerList = generateListInteger();
        assertFalse(integerList.isEmpty());
    }

    @Test
    public void testIterator(){
        List<String> stringList = generateListString();
        Iterator<String> stringIterator = stringList.iterator();
        StringBuilder iteration = new StringBuilder();
        do {
            iteration.append(stringIterator.next()).append(", ");
        } while (stringIterator.hasNext());
        String expectedIteration = "String 1, String 2, String 3, String 4, String 5, String 3, ";
        assertEquals(expectedIteration, iteration.toString());


        List<Integer> integerList = generateListInteger();
        Iterator<Integer> integerIterator = integerList.iterator();
        iteration = new StringBuilder();
        do {
            iteration.append(integerIterator.next()).append(", ");
        } while (integerIterator.hasNext());
        expectedIteration = "1, 2, 3, 4, 5, 3, ";
        assertEquals(expectedIteration, iteration.toString());
    }

    @Test
    public void testAdd(){
        List<String> stringList = new JULinkedList<>();
        stringList.add("String 1");
        stringList.add("String 2");
        stringList.add("String 3");
        String waitingList = "{String 1, String 2, String 3}";
        assertEquals(waitingList, ((JULinkedList<String>) stringList).buildList());

        List<Integer> integerList = new JULinkedList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        waitingList = "{1, 2, 3}";
        assertEquals(waitingList, ((JULinkedList<Integer>) integerList).buildList());
    }

    @Test
    public void testRemove(){
        List<String> stringList = generateListString();
        stringList.remove("String 2");
        String waitingList = "{String 1, String 3, String 4, String 5, String 3}";
        assertEquals(waitingList, ((JULinkedList<String>) stringList).buildList());

        List<Integer> integerList = generateListInteger();
        integerList.remove((Object)2);
        waitingList = "{1, 3, 4, 5, 3}";
        assertEquals(waitingList, ((JULinkedList<Integer>) integerList).buildList());
    }

    @Test
    public void testRemoveByIndex(){
        List<String> stringList = generateListString();
        stringList.remove(2);
        String waitingList = "{String 1, String 2, String 4, String 5, String 3}";
        assertEquals(waitingList, ((JULinkedList<String>) stringList).buildList());

        List<Integer> integerList = generateListInteger();
        integerList.remove(2);
        waitingList = "{1, 2, 4, 5, 3}";
        assertEquals(waitingList, ((JULinkedList<Integer>) integerList).buildList());
    }

    @Test
    public void testClear(){
        List<String> stringList = generateListString();
        stringList.clear();
        String waitingList = "{}";
        assertEquals(waitingList, ((JULinkedList<String>) stringList).buildList());

        List<Integer> integerList = generateListInteger();
        integerList.clear();
        waitingList = "{}";
        assertEquals(waitingList, ((JULinkedList<Integer>) integerList).buildList());
    }

    @Test
    public void testContains(){
        List<String> stringList = generateListString();
        assertTrue(stringList.contains("String 2"));

        List<Integer> integerList = generateListInteger();
        assertTrue(integerList.contains(2));
    }

    @Test
    public void testSet(){
        List<String> stringList = generateListString();
        stringList.set(0, "String 1000");
        assertTrue(stringList.contains("String 1000"));

        List<Integer> integerList = generateListInteger();
        integerList.set(0, 1000);
        assertTrue(integerList.contains(1000));
    }

    @Test
    public void testIndexOf(){
        List<String> stringList = generateListString();
        assertEquals(2, stringList.indexOf("String 3"));

        List<Integer> integerList = generateListInteger();
        assertEquals(2, integerList.indexOf(3));
    }

    @Test
    public void testLastIndexOf(){
        List<String> stringList = generateListString();
        assertEquals(5, stringList.lastIndexOf("String 3"));

        List<Integer> integerList = generateListInteger();
        assertEquals(5, integerList.lastIndexOf(3));
    }

}
