import OrderedLists_Task12.Bag;
import OrderedLists_Task12.LinkedBag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task12Test {

    private LinkedBag<Integer> generateList(){
        LinkedBag<Integer> linkedBag = new LinkedBag<>();
        linkedBag.add(2);
        linkedBag.add(5);
        linkedBag.add(1);
        linkedBag.add(4);
        linkedBag.add(3);
        return linkedBag;
    }

    @Test
    public void testSelectionSort(){
        Bag<Integer> list = generateList();
        list.selectionSort();
        String expectedList = "(5)root={data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->null}}}}}";
        assertEquals(expectedList, list.toString());
    }

    @Test
    public void testBubbleSort(){
        Bag<Integer> list = generateList();
        list.bubbleSort();
        String expectedList = "(5)root={data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->null}}}}}";
        assertEquals(expectedList, list.toString());
    }

    @Test
    public void testInsertionSort(){
        Bag<Integer> list = generateList();
        list.insertionSort();
        String expectedList = "(5)root={data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->null}}}}}";
        assertEquals(expectedList, list.toString());
    }
}
