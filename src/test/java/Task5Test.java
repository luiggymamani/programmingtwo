import RotateArray_task5.RotateLogic;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class Task5Test {

    RotateLogic rotateLogic = new RotateLogic();
    int[] mainArray = {1, 2, 3, 4, 5};

    @Test
    public void testPositiveRotation() {
        int[] expectedArray;
        int[] rotatedArray;

        expectedArray = new int[]{2, 3, 4, 5, 1};
        rotatedArray = rotateLogic.rotate(mainArray, 4);
        assertArrayEquals(expectedArray, rotatedArray);

        expectedArray = new int[]{3, 4, 5, 1, 2};
        rotatedArray = rotateLogic.rotate(mainArray, 12478);
        assertArrayEquals(expectedArray, rotatedArray);
    }

    @Test
    public void testNegativeRotation() {
        int[] expectedArray;
        int[] rotatedArray;

        expectedArray = new int[]{5, 1, 2, 3, 4};
        rotatedArray = rotateLogic.rotate(mainArray, -4);
        assertArrayEquals(expectedArray, rotatedArray);

        expectedArray = new int[]{4, 5, 1, 2, 3};
        rotatedArray = rotateLogic.rotate(mainArray, -12478);
        assertArrayEquals(expectedArray, rotatedArray);
    }

    @Test
    public void testRotationWithZero() {
        int[] expectedArray = {1, 2, 3, 4, 5};
        int[] rotatedArray = rotateLogic.rotate(mainArray, 0);

        assertArrayEquals(expectedArray, rotatedArray);
    }
}
