import DoublyLinkedList_Task13.DoublyLinkedList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Task13Test {

    private DoublyLinkedList<Integer> generateList() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.add(1); list.add(2); list.add(3); list.add(4); list.add(5);
        return list;
    }

    private DoublyLinkedList<String> generateListString(){
        DoublyLinkedList<String> list = new DoublyLinkedList<>();
        list.add("String 1"); list.add("String 2"); list.add("String 3"); list.add("String 4"); list.add("String 5");
        return list;
    }

    @Test
    public void testAddPlain(){
        DoublyLinkedList<Integer> list = generateList();
        assertTrue(list.add(6));
        assertTrue(list.add(7));
        assertTrue(list.add(8));
        String waitingList = "1  <==>  2  <==>  3  <==>  4  <==>  5  <==>  6  <==>  7  <==>  8  <==>  null";
        assertEquals(waitingList, list.toString());

        DoublyLinkedList<String> listString = generateListString();
        assertTrue(listString.add("String 6"));
        assertTrue(listString.add("String 7"));
        assertTrue(listString.add("String 8"));
        waitingList = "String 1  <==>  String 2  <==>  String 3  <==>  String 4  <==>  String 5  <==>  String 6  <==>  String 7  <==>  String 8  <==>  null";
        assertEquals(waitingList, listString.toString());
    }

    @Test
    public void testAddNodePointers(){
        DoublyLinkedList<Integer> list = generateList();

        // Expected:  null <==> 1 <==> 2
        assertNull(list.getNode(0).getPreviousNode());
        assertEquals(2, list.getNode(0).getNextNode().getNodeData());

        // Expected:  2 <==> 3 <==> 4
        assertEquals(2, list.getNode(2).getPreviousNode().getNodeData());
        assertEquals(4, list.getNode(2).getNextNode().getNodeData());

        // Expected:  4 <==> 5 <==> null
        assertEquals(4, list.getNode(4).getPreviousNode().getNodeData());
        assertNull(list.getNode(4).getNextNode());

        DoublyLinkedList<String> listString = generateListString();
        // Expected:  null <==> String 1 <==> String 2
        assertNull(listString.getNode(0).getPreviousNode());
        assertEquals("String 2", listString.getNode(0).getNextNode().getNodeData());
        // Expected:  String 2 <==> String 3 <==> String 4
        assertEquals("String 2", listString.getNode(2).getPreviousNode().getNodeData());
        assertEquals("String 4", listString.getNode(2).getNextNode().getNodeData());
        // Expected:  String 4 <==> String 5 <==> null
        assertEquals("String 4", listString.getNode(4).getPreviousNode().getNodeData());
        assertNull(listString.getNode(4).getNextNode());
    }

    @Test
    public void testRemovePlain(){
        DoublyLinkedList<Integer> list = generateList();
        assertTrue(list.remove(3));
        assertTrue(list.remove(5));
        assertTrue(list.remove(1));

        String waitingList = "2  <==>  4  <==>  null";
        assertEquals(waitingList, list.toString());

        DoublyLinkedList<String> listString = generateListString();
        assertTrue(listString.remove("String 3"));
        assertTrue(listString.remove("String 5"));
        assertTrue(listString.remove("String 1"));

        waitingList = "String 2  <==>  String 4  <==>  null";
        assertEquals(waitingList, listString.toString());
    }

    @Test
    public void testRemoveNodePointers(){
        DoublyLinkedList<Integer> list = generateList();

        // Expected:  unmodifiedNode  <==>  2  <==>  4  <==>  unmodifiedNode
        assertTrue(list.remove(3));
        assertEquals(2, list.getNode(2).getPreviousNode().getNodeData());
        assertEquals(4, list.getNode(1).getNextNode().getNodeData());

        // Expected:  unmodifiedNode <==> 4 <==> null
        assertTrue(list.remove(5));
        assertNull(list.getNode(2).getNextNode());

        // Expected:  null <==> 2 <==> unmodifiedNode
        assertTrue(list.remove(1));
        assertNull(list.getNode(0).getPreviousNode());

        DoublyLinkedList<String> listString = generateListString();
        // Expected:  unmodifiedNode  <==>  String 2  <==>  String 4  <==>  unmodifiedNode
        assertTrue(listString.remove("String 3"));
        assertEquals("String 2", listString.getNode(2).getPreviousNode().getNodeData());
        assertEquals("String 4", listString.getNode(1).getNextNode().getNodeData());
        // Expected:  unmodifiedNode <==> String 4 <==> null
        assertTrue(listString.remove("String 5"));
        assertNull(listString.getNode(2).getNextNode());
        // Expected:  null <==> String 2 <==> unmodifiedNode
        assertTrue(listString.remove("String 1"));
        assertNull(listString.getNode(0).getPreviousNode());
    }

    @Test
    public void testAddAndRemove(){
        DoublyLinkedList<Integer> list = generateList();

        // Expected:  unmodifiedNode <==> 4 <==> null
        assertTrue(list.remove(5));
        assertNull(list.getNode(3).getNextNode());

        // Expected:  4 <==> 100 <==> null
        assertTrue(list.add(100));
        assertEquals(100, list.get(4));
        assertEquals(4, list.getNode(4).getPreviousNode().getNodeData());
        assertNull(list.getNode(4).getNextNode());

        String waitingList = "1  <==>  2  <==>  3  <==>  4  <==>  100  <==>  null";
        assertEquals(waitingList, list.toString());

        assertTrue(list.add(100));
        assertTrue(list.add(100));
        assertTrue(list.remove(100));
        waitingList = "1  <==>  2  <==>  3  <==>  4  <==>  100  <==>  100  <==>  null";
        assertEquals(waitingList, list.toString());
        assertEquals(100, list.get(4));
        assertEquals(100, list.get(5));
        assertEquals(4, list.getNode(4).getPreviousNode().getNodeData());
        assertEquals(100, list.getNode(5).getPreviousNode().getNodeData());
        assertNull(list.getNode(5).getNextNode());

        assertFalse(list.remove(235));
        assertTrue(list.remove(1));
        assertTrue(list.remove(2));
        assertTrue(list.remove(3));
        assertTrue(list.remove(4));
        assertTrue(list.remove(100));
        assertTrue(list.remove(100));
        assertFalse(list.remove(352));

        waitingList = "null";
        assertEquals(waitingList, list.toString());

        DoublyLinkedList<String> listString = generateListString();
        // Expected:  unmodifiedNode <==> String 4 <==> null
        assertTrue(listString.remove("String 5"));
        assertNull(listString.getNode(3).getNextNode());
        // Expected:  String 4 <==> String 100 <==> null
        assertTrue(listString.add("String 100"));
        assertEquals("String 100", listString.get(4));
        assertEquals("String 4", listString.getNode(4).getPreviousNode().getNodeData());
        assertNull(listString.getNode(4).getNextNode());

        waitingList = "String 1  <==>  String 2  <==>  String 3  <==>  String 4  <==>  String 100  <==>  null";
        assertEquals(waitingList, listString.toString());

        assertTrue(listString.add("String 100"));
        assertTrue(listString.add("String 100"));
        assertTrue(listString.remove("String 100"));
        waitingList = "String 1  <==>  String 2  <==>  String 3  <==>  String 4  <==>  String 100  <==>  String 100  <==>  null";
        assertEquals(waitingList, listString.toString());
        assertEquals("String 100", listString.get(4));
        assertEquals("String 100", listString.get(5));
        assertEquals("String 4", listString.getNode(4).getPreviousNode().getNodeData());
        assertEquals("String 100", listString.getNode(5).getPreviousNode().getNodeData());
        assertNull(listString.getNode(5).getNextNode());

        assertFalse(listString.remove("String 235"));
        assertTrue(listString.remove("String 1"));
        assertTrue(listString.remove("String 2"));
        assertTrue(listString.remove("String 3"));
        assertTrue(listString.remove("String 4"));
        assertTrue(listString.remove("String 100"));
        assertTrue(listString.remove("String 100"));
        assertFalse(listString.remove("String 352"));
        waitingList = "null";
        assertEquals(waitingList, listString.toString());
    }

    @Test
    public void testXChangeInteger(){
        DoublyLinkedList<Integer> listInteger = generateList();

        listInteger.xchange(0, 4);
        String waitingList = "5  <==>  2  <==>  3  <==>  4  <==>  1  <==>  null";
        assertEquals(waitingList, listInteger.toString());
        // Expected:  null  <==>  5  <==>  2
        assertNull(listInteger.getNode(0).getPreviousNode());
        assertEquals(2, listInteger.getNode(0).getNextNode().getNodeData());
        assertEquals(5, listInteger.getNode(1).getPreviousNode().getNodeData());
        // Expected:  4  <==>  1  <==> null
        assertEquals(4, listInteger.getNode(4).getPreviousNode().getNodeData());
        assertNull(listInteger.getNode(4).getNextNode());
        assertEquals(1, listInteger.getNode(3).getNextNode().getNodeData());


        listInteger.xchange(1, 3);
        waitingList = "5  <==>  4  <==>  3  <==>  2  <==>  1  <==>  null";
        assertEquals(waitingList, listInteger.toString());
        // Expected:  5  <==>  4  <==>  3
        assertEquals(5, listInteger.getNode(1).getPreviousNode().getNodeData());
        assertEquals(3, listInteger.getNode(1).getNextNode().getNodeData());
        assertEquals(4, listInteger.getNode(2).getPreviousNode().getNodeData());
        assertEquals(4, listInteger.getNode(0).getNextNode().getNodeData());
        // Expected:  3  <==>  2  <==> 1
        assertEquals(3, listInteger.getNode(3).getPreviousNode().getNodeData());
        assertEquals(1, listInteger.getNode(3).getNextNode().getNodeData());
        assertEquals(2, listInteger.getNode(4).getPreviousNode().getNodeData());
        assertEquals(2, listInteger.getNode(2).getNextNode().getNodeData());


        // Without changes
        listInteger.xchange(1, 8);
        waitingList = "5  <==>  4  <==>  3  <==>  2  <==>  1  <==>  null";
        assertEquals(waitingList, listInteger.toString());


        listInteger.xchange(2, 4);
        waitingList = "5  <==>  4  <==>  1  <==>  2  <==>  3  <==>  null";
        assertEquals(waitingList, listInteger.toString());
        // Expected:  4  <==>  1  <==>  2
        assertEquals(4, listInteger.getNode(2).getPreviousNode().getNodeData());
        assertEquals(2, listInteger.getNode(2).getNextNode().getNodeData());
        assertEquals(1, listInteger.getNode(3).getPreviousNode().getNodeData());
        assertEquals(1, listInteger.getNode(1).getNextNode().getNodeData());
        // Expected:  2  <==>  3  <==> null
        assertEquals(2, listInteger.getNode(4).getPreviousNode().getNodeData());
        assertNull(listInteger.getNode(4).getNextNode());
        assertEquals(3, listInteger.getNode(3).getNextNode().getNodeData());
    }

    @Test
    public void testXChangeString(){
        DoublyLinkedList<String> listString = generateListString();

        listString.xchange(0, 4);
        String waitingList = "String 5  <==>  String 2  <==>  String 3  <==>  String 4  <==>  String 1  <==>  null";
        assertEquals(waitingList, listString.toString());
        // Expected:  null  <==>  String 5  <==>  String 2
        assertNull(listString.getNode(0).getPreviousNode());
        assertEquals("String 2", listString.getNode(0).getNextNode().getNodeData());
        assertEquals("String 5", listString.getNode(1).getPreviousNode().getNodeData());
        // Expected:  String 4  <==>  String 1  <==> null
        assertEquals("String 4", listString.getNode(4).getPreviousNode().getNodeData());
        assertNull(listString.getNode(4).getNextNode());
        assertEquals("String 1", listString.getNode(3).getNextNode().getNodeData());


        listString.xchange(1, 3);
        waitingList = "String 5  <==>  String 4  <==>  String 3  <==>  String 2  <==>  String 1  <==>  null";
        assertEquals(waitingList, listString.toString());
        // Expected:  String 5  <==>  String 4  <==>  String 3
        assertEquals("String 5", listString.getNode(1).getPreviousNode().getNodeData());
        assertEquals("String 3", listString.getNode(1).getNextNode().getNodeData());
        assertEquals("String 4", listString.getNode(2).getPreviousNode().getNodeData());
        assertEquals("String 4", listString.getNode(0).getNextNode().getNodeData());
        // Expected:  String 3  <==>  String 2  <==> String 1
        assertEquals("String 3", listString.getNode(3).getPreviousNode().getNodeData());
        assertEquals("String 1", listString.getNode(3).getNextNode().getNodeData());
        assertEquals("String 2", listString.getNode(4).getPreviousNode().getNodeData());
        assertEquals("String 2", listString.getNode(2).getNextNode().getNodeData());


        // Without changes
        listString.xchange(1, 8);
        waitingList = "String 5  <==>  String 4  <==>  String 3  <==>  String 2  <==>  String 1  <==>  null";
        assertEquals(waitingList, listString.toString());


        listString.xchange(2, 4);
        waitingList = "String 5  <==>  String 4  <==>  String 1  <==>  String 2  <==>  String 3  <==>  null";
        assertEquals(waitingList, listString.toString());
        // Expected:  String 4  <==>  String 1  <==>  String 2
        assertEquals("String 4", listString.getNode(2).getPreviousNode().getNodeData());
        assertEquals("String 2", listString.getNode(2).getNextNode().getNodeData());
        assertEquals("String 1", listString.getNode(3).getPreviousNode().getNodeData());
        assertEquals("String 1", listString.getNode(1).getNextNode().getNodeData());
        // Expected:  String 2  <==>  String 3  <==> null
        assertEquals("String 2", listString.getNode(4).getPreviousNode().getNodeData());
        assertNull(listString.getNode(4).getNextNode());
        assertEquals("String 3", listString.getNode(3).getNextNode().getNodeData());
    }
}
