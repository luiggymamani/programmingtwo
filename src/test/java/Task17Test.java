import Stack_Task16.Calculator;
import Stack_Task16.CalculatorValidator;
import Stack_Task16.Operations;
import Stack_Task16.Stack;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Task17Test {

    @Test
    public void testSum() {
        Calculator calculator = new Calculator();

        int expectedResult = 100;
        assertEquals(expectedResult, calculator.solveOperation("45+55"));
        expectedResult = 150;
        assertEquals(expectedResult, calculator.solveOperation("45+55+35+15"));
        expectedResult = 42;
        assertEquals(expectedResult, calculator.solveOperation("(2+28)+(10+2)"));
        expectedResult = 254;
        assertEquals(expectedResult, calculator.solveOperation("(45+55+35+15)+(10+5+21+68)"));
    }

    @Test
    public void testMultiplication() {
        Calculator calculator = new Calculator();

        int expectedResult = 75;
        assertEquals(expectedResult, calculator.solveOperation("3*25"));
        expectedResult = 112;
        assertEquals(expectedResult, calculator.solveOperation("2*2*4*7"));
        expectedResult = 20;
        assertEquals(expectedResult, calculator.solveOperation("5*(2*2)"));
        expectedResult = 50;
        assertEquals(expectedResult, calculator.solveOperation("(2*(5*5))"));
        expectedResult = 12000;
        assertEquals(expectedResult, calculator.solveOperation("(3*(10*2)*2)*10*10"));
        expectedResult = 240000;
        assertEquals(expectedResult, calculator.solveOperation("(3*5*2*1*4)*(500*2)*2"));
    }

    @Test
    public void testPotency() {
        Calculator calculator = new Calculator();

        int expectedResult = 729;
        assertEquals(expectedResult, calculator.solveOperation("9^3"));
        expectedResult = 256;
        assertEquals(expectedResult, calculator.solveOperation("2^2^4"));
        expectedResult = 625;
        assertEquals(expectedResult, calculator.solveOperation("5^(2^2)"));
        expectedResult = 1024;
        assertEquals(expectedResult, calculator.solveOperation("(2^5)^2"));
    }

    @Test
    public void testFactorial() {
        Calculator calculator = new Calculator();

        int expectedResult = 120;
        assertEquals(expectedResult, calculator.solveOperation("5!"));
        expectedResult = 720;
        assertEquals(expectedResult, calculator.solveOperation("(3!)!"));
    }

    @Test
    public void testCombinedOperations() {
        Calculator calculator = new Calculator();

        int expectedResult = 10;
        assertEquals(expectedResult, calculator.solveOperation("2^3+2"));
        expectedResult = 15;
        assertEquals(expectedResult, calculator.solveOperation("1+2*5"));
        expectedResult = 48;
        assertEquals(expectedResult, calculator.solveOperation("4^2*3"));
        expectedResult = 120;
        assertEquals(expectedResult, calculator.solveOperation("2+3!"));
        expectedResult = 4096;
        assertEquals(expectedResult, calculator.solveOperation("4^(2*3)"));
        expectedResult = 36;
        assertEquals(expectedResult, calculator.solveOperation("3!^2"));
        expectedResult = 739;
        assertEquals(expectedResult, calculator.solveOperation("3^(3!)+(5*2)"));
        expectedResult = 1468;
        assertEquals(expectedResult, calculator.solveOperation("((3^(3!))+5)*2"));
        expectedResult = 144;
        assertEquals(expectedResult, calculator.solveOperation("((2^(2!))+5)*(2+2)*4"));
        expectedResult = 1230;
        assertEquals(expectedResult, calculator.solveOperation("((3^2)+5)*2+1202"));

    }

    @Test
    public void testExceptions() {
        Calculator calculator = new Calculator();
        int expectedResult = 0;
        assertEquals(expectedResult, calculator.solveOperation("1000!"));
        assertEquals(expectedResult, calculator.solveOperation("6/3-1"));
        assertEquals(expectedResult, calculator.solveOperation("(((3^(3!))+5)*-2))"));
        assertEquals(expectedResult, calculator.solveOperation("2147483647+1"));
        // Para estas excepciones se muestran mensajes en la consola.
    }

    @Test
    public void testMethodFindNumber() {
        Calculator calculator = new Calculator();
        Stack<Character> stack = new Stack<>();
        stack.push('2'); stack.push('3'); stack.push('5');
        stack.push('+'); stack.push('2');
        int expectedResult = 235;

        // este proceso ya lo hace otro metodo pero es privado
        Stack<Character> invertedStack = new Stack<>();
        while (!stack.isEmpty())
            invertedStack.push(stack.pop());
        char character = invertedStack.pop();

        assertEquals(expectedResult, calculator.findNumber(character, invertedStack));


        Stack<Character> stack2 = new Stack<>();
        stack2.push('1');
        stack2.push('0');
        stack2.push('2');
        stack2.push('0');
        expectedResult = 1020;

        Stack<Character> invertedStack2 = new Stack<>();
        while (!stack2.isEmpty())
            invertedStack2.push(stack2.pop());
        character = invertedStack2.pop();

        assertEquals(expectedResult, calculator.findNumber(character, invertedStack2));
    }

    @Test
    public void testValidator() {
        CalculatorValidator validator = new CalculatorValidator();

        assertTrue(validator.isValid("2+2*4"));
        assertTrue(validator.validateOperators());

        assertTrue(validator.isValid("2+(2*4)"));
        assertTrue(validator.validateOperators());
        assertTrue(validator.validateParentheses());

        assertTrue(validator.isValid("(2+2)*4"));
        assertTrue(validator.validateOperators());
        assertTrue(validator.validateParentheses());

        assertTrue(validator.isValid("(2+2+4)"));
        assertTrue(validator.validateOperators());
        assertTrue(validator.validateParentheses());

        assertTrue(validator.isValid("2+(2+4)*5+6+(9+3)"));
        assertTrue(validator.validateOperators());
        assertTrue(validator.validateParentheses());

        // Operaciones no validas

        assertFalse(validator.isValid("((2+2"));
        assertFalse(validator.validateParentheses());

        assertFalse(validator.isValid("2+-2"));
        assertFalse(validator.validateOperators());

        assertFalse(validator.isValid("10/2"));
        assertFalse(validator.validateOperators());

        assertFalse(validator.isValid("500-100"));
        assertFalse(validator.validateOperators());

        assertFalse(validator.isValid("5!)-56"));
        assertFalse(validator.validateParentheses());
        assertFalse(validator.validateOperators());
    }

    @Test
    public void testOperationsClass() {
        Operations operations = new Operations();

        int expectedResult = 6741;
        assertEquals(expectedResult, operations.addition(153, 6588));
        expectedResult = 943;
        assertEquals(expectedResult, operations.addition(943, 0));

        expectedResult = 693;
        assertEquals(expectedResult, operations.multiplication(11, 63));
        expectedResult = 1150;
        assertEquals(expectedResult, operations.multiplication(50, 23));

        expectedResult = 1600;
        assertEquals(expectedResult, operations.power(40, 2));
        expectedResult = 256;
        assertEquals(expectedResult, operations.power(2, 8));

        expectedResult = 5040;
        assertEquals(expectedResult, operations.factorial(7));
        expectedResult = 120;
        assertEquals(expectedResult, operations.factorial(5));
    }
}
