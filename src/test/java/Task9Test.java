import GetNodo_Task9.Nodo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Task9Test {

    @Test
    public void test2() {
        Nodo mainNode = new Nodo(1337);
        Nodo secondNode = new Nodo(42);
        Nodo thirdNode = new Nodo(23);
        mainNode.setNextNode(secondNode);
        mainNode.getNextNode().setNextNode(thirdNode);

        assertEquals(1337, Nodo.getNth(mainNode, 0));
        assertEquals(42, Nodo.getNth(mainNode, 1));
        assertEquals(23, Nodo.getNth(mainNode, 2));
    }

    @Test
    public void testNull() {
        try{
            Nodo.getNth(null, 0);
            fail();
        }catch(Exception e){
            assertTrue(true);
        }
    }

    @Test
    public void testWrongIdx() {
        try{
            Nodo.getNth(new Nodo(), 1);
            fail();
        }catch(Exception e){
            assertTrue(true);
        }
    }
}
