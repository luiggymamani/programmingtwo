import Stack_Task16.Stack;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task16Test {

    @Test
    public void testPush(){
        Stack<Integer> integerStack = new Stack<>();
        integerStack.push(100);
        integerStack.push(200);
        integerStack.push(300);
        integerStack.push(400);
        integerStack.push(500);
        String expectedStack = "{1. 100, 2. 200, 3. 300, 4. 400, 5. 500}";
        assertEquals(expectedStack, integerStack.buildStack());

        Stack<String> stringStack = new Stack<>();
        stringStack.push("String 1");
        stringStack.push("String 2");
        stringStack.push("String 3");
        stringStack.push("String 4");
        stringStack.push("String 5");
        expectedStack = "{1. String 1, 2. String 2, 3. String 3, 4. String 4, 5. String 5}";
        assertEquals(expectedStack, stringStack.buildStack());
    }

    @Test
    public void testPop(){
        Stack<Integer> integerStack = new Stack<>();
        integerStack.push(100);
        integerStack.push(200);
        integerStack.push(300);

        integerStack.pop();
        String expectedStack = "{1. 100, 2. 200}";
        assertEquals(expectedStack, integerStack.buildStack());

        integerStack.pop();
        expectedStack = "{1. 100}";
        assertEquals(expectedStack, integerStack.buildStack());

        integerStack.pop();
        expectedStack = "{}";
        assertEquals(expectedStack, integerStack.buildStack());

        Stack<String> stringStack = new Stack<>();
        stringStack.push("String 1");
        stringStack.push("String 2");
        stringStack.push("String 3");

        stringStack.pop();
        expectedStack = "{1. String 1, 2. String 2}";
        assertEquals(expectedStack, stringStack.buildStack());

        stringStack.pop();
        expectedStack = "{1. String 1}";
        assertEquals(expectedStack, stringStack.buildStack());

        stringStack.pop();
        expectedStack = "{}";
        assertEquals(expectedStack, stringStack.buildStack());
    }

    @Test
    public void testPushAndPop(){
        Stack<Integer> integerStack = new Stack<>();
        integerStack.push(100);
        integerStack.push(200);
        integerStack.push(300);

        integerStack.pop();
        String expectedStack = "{1. 100, 2. 200}";
        assertEquals(expectedStack, integerStack.buildStack());

        integerStack.push(500);
        integerStack.push(800);
        expectedStack = "{1. 100, 2. 200, 3. 500, 4. 800}";
        assertEquals(expectedStack, integerStack.buildStack());

        integerStack.pop();
        expectedStack = "{1. 100, 2. 200, 3. 500}";
        assertEquals(expectedStack, integerStack.buildStack());

        Stack<String> stringStack = new Stack<>();
        stringStack.push("String 1");
        stringStack.push("String 2");
        stringStack.push("String 3");

        stringStack.pop();
        expectedStack = "{1. String 1, 2. String 2}";
        assertEquals(expectedStack, stringStack.buildStack());

        stringStack.push("String 5");
        stringStack.push("String 8");
        expectedStack = "{1. String 1, 2. String 2, 3. String 5, 4. String 8}";
        assertEquals(expectedStack, stringStack.buildStack());

        stringStack.pop();
        expectedStack = "{1. String 1, 2. String 2, 3. String 5}";
        assertEquals(expectedStack, stringStack.buildStack());
    }
}
