import GameArray_Task6.Map;
import GameArray_Task6.Player;
import GameArray_Task6.PlayerMoves;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Task6Test {

    Player player = new Player("Luiggy", " x ");
    Map map = new Map(10, 5);

    private Map generateMap(){
        map.fillTheMap();
        map.insertPlayer(5, 5, player);
        map.insertObstacle(3, 3);
        map.insertObstacle(4, 1);
        map.insertObstacle(8, 2);
        map.insertObstacle(2, 9);
        map.insertObstacle(6, 9);
        return map;
    }


    @Test
    public void testMovePlayerUp(){
        PlayerMoves playerMoves = new PlayerMoves(player, generateMap());
        playerMoves.movePlayerUp(5, 5);
        String[][] arrayToCompare = {{" . "," . "," . "," . "," . "," . "," . "," . "," . "," . "},
                                     {" . "," . "," . "," . "," # "," . "," . "," . "," . "," . ",},
                                     {" . "," . "," . "," . "," . "," . "," . "," . "," # "," . ",},
                                     {" . "," . "," . "," # "," . "," . "," . "," . "," . "," . ",},
                                     {" . "," . "," . "," . "," . "," x "," . "," . "," . "," . ",},
                                     {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                     {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                     {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                     {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                     {" . "," . "," # "," . "," . "," . "," # "," . "," . "," . ",}};

        assertArrayEquals(arrayToCompare, map.getArrayMap());
    }

    @Test
    public void testMovePlayerDown(){
        PlayerMoves playerMoves = new PlayerMoves(player, generateMap());
        playerMoves.movePlayerDown(5, 5);
        String[][] arrayToCompare = {{" . "," . "," . "," . "," . "," . "," . "," . "," . "," . "},
                                    {" . "," . "," . "," . "," # "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," # "," . ",},
                                    {" . "," . "," . "," # "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," x "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," # "," . "," . "," . "," # "," . "," . "," . ",}};

        assertArrayEquals(arrayToCompare, map.getArrayMap());
    }

    @Test
    public void testMovePlayerRight(){
        PlayerMoves playerMoves = new PlayerMoves(player, generateMap());
        playerMoves.movePlayerRight(5, 5);
        String[][] arrayToCompare = {{" . "," . "," . "," . "," . "," . "," . "," . "," . "," . "},
                                    {" . "," . "," . "," . "," # "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," # "," . ",},
                                    {" . "," . "," . "," # "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," x "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," # "," . "," . "," . "," # "," . "," . "," . ",}};

        assertArrayEquals(arrayToCompare, map.getArrayMap());
    }

    @Test
    public void testMovePlayerLeft(){
        PlayerMoves playerMoves = new PlayerMoves(player, generateMap());
        playerMoves.movePlayerLeft(5, 5);
        String[][] arrayToCompare = {{" . "," . "," . "," . "," . "," . "," . "," . "," . "," . "},
                                    {" . "," . "," . "," . "," # "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," # "," . ",},
                                    {" . "," . "," . "," # "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," x "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," # "," . "," . "," . "," # "," . "," . "," . ",}};

        assertArrayEquals(arrayToCompare, map.getArrayMap());
    }

    @Test
    public void testHitAnObstacle(){
        PlayerMoves playerMoves = new PlayerMoves(player, generateMap());
        playerMoves.movePlayerUp(5, 5);
        playerMoves.movePlayerUp(5, 4);

        playerMoves.movePlayerLeft(5, 3);
        playerMoves.movePlayerLeft(4, 3);

        String[][] arrayToCompare = {{" . "," . "," . "," . "," . "," . "," . "," . "," . "," . "},
                                    {" . "," . "," . "," . "," # "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," # "," . ",},
                                    {" . "," . "," . "," # "," x "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," # "," . "," . "," . "," # "," . "," . "," . ",}};

        assertArrayEquals(arrayToCompare, map.getArrayMap());
    }

    @Test
    public void testHitTheLimit(){
        PlayerMoves playerMoves = new PlayerMoves(player, generateMap());
        playerMoves.movePlayerUp(5, 5);
        playerMoves.movePlayerUp(5, 4);
        playerMoves.movePlayerUp(5, 3);
        playerMoves.movePlayerUp(5, 2);
        playerMoves.movePlayerUp(5, 1);
        playerMoves.movePlayerUp(5, 0);

        String[][] arrayToCompare = {{" . "," . "," . "," . "," . "," x "," . "," . "," . "," . "},
                                    {" . "," . "," . "," . "," # "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," # "," . ",},
                                    {" . "," . "," . "," # "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," . "," . "," . "," . "," . "," . "," . "," . ",},
                                    {" . "," . "," # "," . "," . "," . "," # "," . "," . "," . ",}};

        assertArrayEquals(arrayToCompare, map.getArrayMap());
    }

    @Test
    public void testWinningGame(){
        PlayerMoves playerMoves = new PlayerMoves(player, generateMap());
        playerMoves.movePlayerRight(5, 5);
        playerMoves.movePlayerRight(6, 5);
        playerMoves.movePlayerRight(7, 5);
        playerMoves.movePlayerRight(8, 5);

        playerMoves.movePlayerDown(9, 5);
        playerMoves.movePlayerDown(9, 6);
        playerMoves.movePlayerDown(9, 7);
        playerMoves.movePlayerDown(9, 8);

        int[] positionPlayer = playerMoves.findPlayer();
        assertTrue(map.checkWinningPosition(positionPlayer[0], positionPlayer[1]));
    }
}
